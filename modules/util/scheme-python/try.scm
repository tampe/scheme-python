(define-module (util scheme-python try)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python yield)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (oop goops)
  #:use-module ((util scheme-python oop dict) #:select (slask-it))
  #:use-module (ice-9 control)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:replace (raise)
  #:export (try raise raise-stx exc-info the-exception))

(define exc-info (make-fluid '(None None None)))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-syntax-parameter the-exception (lambda (x) #f))
  
(define (standard-check class obj l)
  (cond
   ((eq? class #t)
    #t)
   ((struct? obj)
    (if (pyclass? obj)
	(class<= class obj)
        (if (pyobject? obj)
            (class<= class (get-class obj))
	    (eq? class obj))))
    ((and (procedure? class) (not (pyclass? class)))
     (apply class obj l))
    (else
     (eq? class obj))))
          
      
(define (check class obj l)
  (standard-check class obj l))

(define-syntax compile-error
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (error (syntax->datum #'x))))))

(define-syntax check-exception
  (syntax-rules (and or not)
    ((_ (or E ...) tag l)
     (or (check-exception E tag l) ...))
    ((_ (and E ...) tag l)
     (and (check-exception E tag l) ...))
    ((_ (not E) tag l)
     (not (check-exception E tag l)))
    ((_ E tag l)
     (check E tag l))))

(define-syntax-rule (mm f)
  (lambda x
    (syntax-parameterize ((the-exception (lambda (u) #'x)))
      (apply f x))))
    
(define-syntax handler
  (lambda (x)
    (syntax-case x ()
      ((_ . l) #'(handler_ . l)))))


(define (m x) x)

(define (get-exc l)
   (if (not (pair? l))
       '(None None None)
       (cons (car l)
             (if (not (pair? (cdr l)))
                 '(cons None None)
                 (cons (cadr l)
                       (if (not (pair? (cddr l)))
                           '(None)
                           (list (caddr l))))))))

(define (get-l tag l)
  (if (eq? tag 'python)
      (begin
        (when (not (= (length l) 3))
          (pk 'get-l l)
          (backtrace))
        l)
      (list SCMError
            (catch #t
              (lambda ()
                (SCMError
                 (format #f
                         "in function ~a, ~a"
                         (car l) (apply format #f (cadr l) (caddr l)))))
              (lambda x
                (SCMError l)))
            #f)))
                                  
                                      

(define-syntax handler_
  (syntax-rules (=>)
    ((handler e ecx)
     (lambda (tag . l)
       (fluid-set! exc-info (get-exc l))
       (set! l   (get-l tag l))
       (set! tag 'python)
       (set! e #t)        
       (handler ecx tag (car l) (cdr l))))
    
    ((handler ((#:except E => lam) . ecx) tag c l)
     (if (check-exception E c l)
         (lam tag (cons c l))
         (handler ecx tag c l)))

    ((handler ((#:except E) . ecx) tag c l)
     (if (check-exception E tag c l)
         (begin (values))
         (handler ecx tag c l)))

    ((handler ((#:except E code ...) . ecx) tag c l)
     (if (check-exception E c l)
         (begin code ...)
         (handler ecx tag c l)))

    ((handler ((#:else code ...)) tag c l)
     (error "No else clause notlast"))
    
    ((handler () tag c l)
     (apply throw tag c l))

    ((a ...)
     (compile-error "not a proper python macro try block"))))


(define inc (let ((i 0)) (lambda () (set! i (+ i 1)) i)))

(define E (gensym "aolsdhj"))
(define-syntax try
  (syntax-rules ()
    ((try code exc ... (#:else els ...) #:finally fin)
     (catch E
       (lambda ()
         (dynamic-wind         
           (lambda () #f)
           (lambda ()
             (let ((e #f))
               (catch #t
                 code
                 (mm (handler e (exc ...))))
               (if e
                   e
                   (begin els ...))))
           (lambda ()
             (if (not (fluid-ref in-yield))
                 (fin)))))
       (lambda x (apply throw x))))
    
    ((try code exc ... #:finally fin)
     (catch E
       (lambda ()
         (dynamic-wind
           (lambda () #f)
           (lambda ()
             (let ((e #t))
               (catch #t
                 code
                 (mm (handler e (exc ...))))))
           (lambda ()
             (if (not (fluid-ref in-yield))
                 (fin)))))
       (lambda x (apply throw x))))

    ((try code exc ... (#:else els ...))
     (catch E
       (lambda ()
         (let ((e #f))
           (catch #t
             code
             (mm (handler e (exc ...))))
           (if e
               e
               (begin els ...))))
       (lambda x (apply throw x))))

    ((try code exc ...)
     (let ((e #t))
       (catch #t
         code
         (mm (handler e (exc ...))))))))
  

(define raise
  (case-lambda
    ((x . l)
     (if (pyclass? x)
         (throw 'python x (apply x l) #f)
         (if (pyobject? x)
             (throw 'python (get-class x) x #f)
             (throw 'python x x #f))))

    (()     
     (raise (Exception "single raise outside exception handler")))))

(define-syntax raise-stx
  (syntax-rules ()
    ((_ x . l)
     (raise x . l))
    ((_)
     (if the-exception
         (let ((l (slask-it the-exception)))
           (apply throw (car l) (get-exc (get-l (car l) (cdr l)))))
         (raise)))))
