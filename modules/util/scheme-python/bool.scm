(define-module (util scheme-python bool)
  #:use-module (oop goops)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python persist)
  #:use-module (util scheme-python oop pf-objects)
  #:export (bool boolean))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-method (bool x)
  (cond
   ((null? x)
    #f)
   ((eq? x None)
    #f)
   (else x)))
(name-object bool)

(define-method (bool (x <integer>)) (if (= x 0) #f x))

(define-method (bool (x <p>))
  (aif it (ref-in-class x '__bool__ #f)
       (it x)
       (next-method)))

(define-method (bool (x <py>))
  (aif it (get-bool (get-class x))
       (it x)
       (next-method)))
		     

(define-method (+ (a <boolean>) b)
  (+ (if a 1 0) b))
(define-method (+ b (a <boolean>))
  (+ (if a 1 0) b))
(define-method (* (a <boolean>) b)
  (* (if a 1 0) b))
(define-method (* b (a <boolean>))
  (* (if a 1 0) b))
(define-method (- (a <boolean>) b)
  (- (if a 1 0) b))
(define-method (- b (a <boolean>))
  (- b (if a 1 0)))

(define-python-class boolean ()
  (define __new__
    (lambda (cls x . l)
      (if (pair? l)
          (let ((self ((rawref object '__new__) cls)))
            (set self '__bool__ (bool x))
            self)
          (bool x)))))

(set boolean '__name__ 'bool)

(define-method (scheme? (o <boolean>)) boolean)

