(define-module (util scheme-python oop pf-objects)
  #:use-module (oop goops)
  #:use-module ((util scheme-python oop dict) #:renamer (lambda (x)
                                       (if (equal? x 'format)
                                           'scm-format
                                           x)))
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:use-module (system base message)
  #:use-module (ice-9 pretty-print)
  #:replace (equal?)
  #:re-export (object-method class-method static-method)
  #:export (set ref make-p <p> <py> <pyc> <pf> <pyf> <property>
                scheme? freeze frozen? get-class get-eq get-hash
                call with copy fset fcall put put! py-get get-bool
                pcall pcall! get fset-x pyclass? kind
                def-p-class   mk-p-class make-p-class mk-p-class2
                mk-py-class mk-py-class2 pyref pyset
                define-python-class define-python-class-noname
		get-type py-class find-in-class               
                py-super-mac py-super py-equal? copy-class
                *class* *self* pyobject? pytype? in
                type object pylist-set! pylist-ref pylist-delete! tr
		resolve-method-g rawref rawset rawdel py-dict
                ref-class fastref fastset ref-in-class pyref
                *pf-pretty* get-goops pyhash-rem! class<=
                ))

(eval-when (compile)
  (format #t "A LOT OF SYMBOL WARINGS WILL RESULT BY THE COPILATION THAT'S OK"))

(eval-when (load eval compile)
  (define is-adv? (module-public-interface
                   (resolve-module '(persist persistance)))))

(eval-when (load eval compile)
  (define (%add-to-warn-list x) x))

(define-syntax mk
  (lambda (x)
    (syntax-case x ()
      ((_)
       (not is-adv?)
       #'(define-syntax-rule (name-object x) (values)))
      ((_)
       #'(use-modules (persist persistance))))))

(mk)

(define *pf-pretty* #t)

(define (property-d x)
  (let ((f (lambda (obj class) (x obj))))
    (rawset x '__get__ f)
    (rawset x '__raw__ f)        
    x))

(define-method (in x (o <hashtable>))
  (not (eq? (hash-ref o x fail) fail)))

#|
Python object system is basically syntactic suger otop of a hashmap and one
this project is inspired by the python object system and what it measn when
one in stead of hasmaps use functional hashmaps. We use vhashes, but those have a drawback in that those are not thread safe. But it is a small effort to work
with assocs or tree like functional hashmaps in stead.

The hashmap works like an assoc e.g. we will define new values by 'consing' a
new binding on the list and when the assoc take up too much space it will be
reshaped and all extra bindings will be removed.

The datastructure is functional but the objects mutate. So one need to 
explicitly tell it to not update etc .
|#

  
(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))


#;
(define (pkk . l)
  (let* ((r (reverse l))
         (x (reverse (cdr r)))
         (z (car r)))
    (apply pk x)
    z))


(define (pkk . l)
  (car (reverse l)))

(define (pk-obj o)
  (pk 'start-pk-obj)
  (let ((h (get-h o)))
    (hash-for-each (lambda (k v)
		     (if (member k '(__name__ __qualname__ __class__))
			 (pk k v)
			 (pk k))) h)
    
    (pk 'finished-obj)
    
    (let lp ((l (pk 'mro (rawref o '__mro__ '()))))
      (if (pair? l)
	  (let ((cl (car l)))
	    (if (is-a? cl <p>)
		(if (hash-table? (get-h cl))
		    (hash-for-each (lambda (k v)
				     (if (member k '(__name__ __qualname__))
					 (pk k v)
					 (pk k)))
				   (slot-ref cl 'h))
		    (pk 'no-hash-table))
		(pk 'no-class))
	    (lp (cdr l)))))

    (pk 'end-pk-obj)))

(define fail (cons 'fail '()))
(name-object fail)

(define-syntax-rule (kif it p x y)
  (let ((it p))
    (if (eq? it fail)
	y
	x)))

(define-method (pylist-set! (o <hashtable>) key val)
  (hash-set! o key val))
(name-object pylist-set!)

(define-method (pylist-ref (o <hashtable>) key)
  (kif it (hash-ref o key fail)
       it
       (error "IndexError")))
(name-object pylist-ref)

(define-method (pylist-delete! (o <hashtable>) k)
  (pyhash-rem! o k))
(name-object pylist-delete!)


(define-method (pyhash-rem! (o <hashtable>) k)
  (hash-remove! o k))

(name-object pylist-delete!)

(define-method (py-get x key . l)
  (if (pair? l) (car l) #f))
(name-object py-get)

(define-method (py-get (o <hashtable>) key . l)
  (define -fail (if (pair? l) (car l) #f))
  (kif it (hash-ref o key fail)
       it
       -fail))

(define (is-acl? a b) (member a (cons b (class-subclasses b))))

(define-class <p>  (<applicable-struct> <object>) h size n)
(define-class <pf> (<p>))                ; the pf object consist of a functional
                                         ; hashmap it's size and number of live
                                         ; object
(define-class <py>  (<p>)  class type mro slots)
(define-class <pyc> (<py>) ref set init new goops hash eq bool geti seti cslots)
(define-class <pyf> (<pf>) class type)


(define (get-h      x) (struct-ref x 1))
(define (get-class  x)
  (cond
   ((struct? x)
    (struct-ref x 4))
     
   ((null? x)
    slask-tuple)
   
   ((procedure? x)
    slask-procedure)))
      
(define (get-type   x) (struct-ref x 5))
(define (get-mro    x) (struct-ref x 6))
(define (get-slots  x) (struct-ref x 7))
(define (get-ref    x) (struct-ref x 8))
(define (get-set    x) (struct-ref x 9))
(define (get-init   x) (struct-ref x 10))
(define (get-new    x) (struct-ref x 11))
(define (get-goops  x) (struct-ref x 12))
(define (get-hash   x) (struct-ref x 13))
(define (get-eq     x) (struct-ref x 14))
(define (get-bool   x) (struct-ref x 15))
(define (get-geti   x) (struct-ref x 16))
(define (get-seti   x) (struct-ref x 17))
(define (get-cslots x) (struct-ref x 18))

(define (class<= a b)
  (let lp ((l (get-mro b)))
    (if (pair? l)
        (or
         (eq? a (car l))
         (lp (cdr l)))
        #f)))

(define (ppcopy x)
  (let ((m (make-hash-table)))
    (hash-for-each
     (lambda (k v)
       (hash-set! m k v))
     x)
    m))

(define (copy-class y)
  (let ((x (make <pyc>)))
    (struct-set! x 1  (ppcopy (struct-ref y 1)))
    (struct-set! x 2  (struct-ref y 2))
    (struct-set! x 3  (struct-ref y 3))
    (struct-set! x 4  (struct-ref y 4))
    (struct-set! x 5  (struct-ref y 5))
    (struct-set! x 6  (struct-ref y 6))
    (struct-set! x 7  (ppcopy (struct-ref y 7)))
    (struct-set! x 8  (struct-ref y 8))
    (struct-set! x 9  (struct-ref y 9))
    (struct-set! x 10 (struct-ref y 10))
    (struct-set! x 11 (struct-ref y 11))
    (struct-set! x 12 (struct-ref y 12))
    (struct-set! x 13 (struct-ref y 13))
    (struct-set! x 14 (struct-ref y 14))
    (struct-set! x 15 (struct-ref y 15))
    (struct-set! x 16 (struct-ref y 16))
    (struct-set! x 17 (struct-ref y 17))
    (struct-set! x 18 (struct-ref y 18))
    x))
  

;(define (get-h      x) (slot-ref x 'h))
;(define (get-class  x) (slot-ref x 'class))
;(define (get-type   x) (slot-ref x 'type))
;(define (get-mro    x) (slot-ref x 'mro))
;(define (get-ref    x) (slot-ref x 'ref))
;(define (get-set    x) (slot-ref x 'set))
;(define (get-init   x) (slot-ref x 'init))
;(define (get-new    x) (slot-ref x 'new))
;(define (get-goops  x) (slot-ref x 'goops))
;(define (get-hash   x) (slot-ref x 'hash))
;(define (get-eq     x) (slot-ref x 'eq))
;(define (get-bool   x) (slot-ref x 'bool))

(define-class <property> () get set del)

(name-object <p>)
(name-object <pf>)
(name-object <py>)
(name-object <pyf>)
(name-object <property>)

(define (fastref o k . e)
  (define r (if (pair? e) (car e) #f))
  (let ((h (get-h o)))
    (cond
     ((hash-table? h)
      (hash-ref (get-h o) k r))
     (else
      (aif it (vhash-assoc k (get-h o)) (cdr it) r)))))

(define (fastset o k v)
  (let ((h (get-h o)))
    (if (hash-table? h)
        (hash-set! (get-h o) k v)
        (slot-set! o 'h
                   (vhash-cons k v (get-h o))))))

(define-method (pylist-set! (o <p>) key val)
  (aif it (ref o '__setitem__)
       (it key val)
       (next-method)))

(define-method (pylist-set! (o <py>) key val)
  (aif it (get-seti (get-class o))
       (it o key val)
       (next-method)))

(define-method (pylist-ref (o <p>) key)
  (aif it (ref o '__getitem__)
       (it key)
       (next-method)))

(define-method (pylist-ref (o <py>) key)
  (aif it (get-geti (get-class o))
       (it o key)
       (next-method)))

(define-method (ref (o <procedure>) key . l)
  (aif it (procedure-property- o key)
       it
       (if (pair? l)
	   (car l)
	   #f)))
(name-object ref)

(define-method (rawref (o <procedure>) key . l)
  (aif it (procedure-property- o key)
       it
       (if (pair? l)
	   (car l)
	   #f)))
(name-object rawref)

(define-method (rawdel (o <procedure>) key)
  (error "not implemented yet"))
(name-object rawdel)

(define-method (set (o <procedure>) key val)
  (set-procedure-property!- o key val))
(name-object set)

(define-method (rawset (o <procedure>) key val)
  (set-procedure-property!- o key val))
(name-object rawset)
  
(define (find-in-class-pf klass key fail)
  (let ((r (vhash-assoc key (get-h klass))))
    (if r
	(cdr r)
	fail)))

(define (find-in-class-p klass key fail)
  (aif it (get-h klass)
       (hash-ref  it key fail)
       fail))

(define inclass (make-fluid #f))
(name-object inclass)

(define (find-in-class-py klass key -fail)
  (let ((slots (get-slots klass)))
    (let ((it (if (hash-table? slots)
                  (hash-ref slots key fail)
                  (if (and (pair? slots) (pair? (car slots)))
                      (aif it (assq key slots) (cdr it) fail)
                      fail))))
      (if (not (eq? it fail))
          it
          (let ((h (get-h klass)))
            (aif dict (hash-ref h '__dict__)
                 (kif it (py-get dict key fail)
                      it
                      (kif it (py-get dict (symbol->string key) fail)
                           it
                           (hash-ref h key -fail)))
                 (hash-ref h key -fail)))))))
      
(define-syntax-rule (find-in-class-and-parents-py klass key fail-)
  (kif r (find-in-class-py klass key fail)
       r
       (aif parents (let ((x (get-mro klass)))
                      (if (null? x)
                          #f
                          x))
            (let lp ((parents parents))           
              (if (pair? parents)
                  (kif r (find-in-class-py (car parents) key fail)
                       r
                       (lp (cdr parents)))
                  fail-))
            fail-)))

(define-syntax-rule (find-in-class-and-parents-p klass key fail-)
  (kif r (find-in-class-p klass key fail)
       r
       (aif parents (let ((x (get-mro klass)))
                      (if (null? x)
                          #f
                          x))
            (let lp ((parents parents))           
              (if (pair? parents)
                  (kif r (find-in-class-p (car parents) key fail)
                       r
                       (lp (cdr parents)))
                  fail-))
            fail-)))

(define-inlinable (ficap klass key fail)
  (find-in-class-and-parents-py klass key fail))

(define-inlinable (ficap-raw klass key fail)
  (find-in-class-and-parents-p klass key fail))

(define (mk-getter-object f)
  (lambda (obj cls)
    (if (eq? obj 'None)
        f
        (lambda x (apply f obj x)))))

(define (mk-getter-static f)
  (lambda (obj cls)
    f))

(define (mk-getter-class f)				       
  (lambda (obj cls)
    (if (eq? obj 'None)
        f
        (lambda x (apply f cls x)))))

(set! class-method
  (lambda (f)
    (set f '__kind__ 'class-method)
    (set f '__get__  (mk-getter-class f))
    f))

(set! object-method
  (lambda (f)
    (set f '__kind__ 'object-method)
    (set f '__get__ (mk-getter-object f))
    f))

(set! static-method
  (lambda (f)
    (set f '__kind__ 'static-method)
    (set f '__get__ (mk-getter-static f))
    f))


(define (resolve-method-g g pattern)
  (define (mmatch p pp)
    (if (eq? pp '_)
	'()
	(match (cons p pp)
	  (((p . ps) . (pp . pps))
	   (if (eq? pp '_)
	       (mmatch ps pps)
	       (if (or (eq? p pp) (is-a? p pp))
		   (cons p (mmatch ps pps))
		   #f)))
	  ((() . ())
	   '())
	  (_
	   #f))))

  (define (q< x y)
    (let lp ((x x) (y y))
      (match (cons x y)
	(((x . xs) . (y . ys))
	 (and (is-a? x y)
	      (lp xs ys)))
	(_ #t))))
  
  (let ((l
	 (let lp ((ms (generic-function-methods g)))
	   (if (pair? ms)
	       (let* ((m (car ms))
		      (p (method-specializers m))
		      (f (method-procedure m)))
		 (aif it (mmatch p pattern)
		     (cons (cons it f) (lp (cdr ms)))
		     (lp (cdr ms))))
	       '()))))
    
    
    (cdr (car (sort l q<)))))

(define (resolve-method-o o pattern)
  (resolve-method-g (class-of o) pattern))

(define (add-default c l)
  (if (pair? l)
      (let ((l (let ((y (car l))) (if (eq? y c) l (cons c l)))))        
        (let* ((r (reverse l))
               (x (car r)))
          (if x
              (if (or (not type) (pytype? x))
                  (if (or (not type) (eq? x type))
                      l
                      (reverse (cons type r)))
                  (if (or (not object) (eq? x object))
                      l
                      (reverse (cons object r))))
              l)))
      (if object
          (if (pytype? c)
              (list c type)
              (list c object))
          (cons c l))))

(define (kw->class kw meta)
  (if (memq #:functional kw)
      (if (memq #:fast kw)
          <pf>
          (if (or (not meta) (is-a? meta <pyf>) (is-a? meta <pyc>))
              <pyf>
              <pf>))              
      (if (memq #:fast kw)
          (if (or (is-a? meta <pyf>) (is-a? meta <pf>))
              <pf>
              <p>)
          (cond
           ((is-a? meta <pyf>)
            <pyf>)
           ((is-a? meta <pyc>)
            <py>)
           ((is-a? meta <pf>)
            <pf>)
           ((is-a? meta <p>)
            <p>)
           (else
            <py>)))))

(define (project-goopses supers)
  (map (lambda (sups)
         (aif it (if (is-a? sups <pyc>)
                     (get-goops sups)
                     sups)
              it
              sups))
       supers))

(define (filter-parents l)
  (let lp ((l l))
    (if (pair? l)
        (if (is-a? (car l) <p>)
            (cons (car l) (lp (cdr l)))
            (lp (cdr l)))
        '())))

(define (get-goops2 meta name parents kw)
  (define (unique l)
    (define t (make-hash-table))
    (let lp ((l l))
      (if (pair? l)
          (let ((c (car l)))
            (if (hashq-ref t c)
                (lp (cdr l))
                (begin
                  (hashq-set! t c #t)
                  (cons c (lp (cdr l))))))
          '())))
   (let ((goops
          (make-class
           (unique
            (append
             (project-goopses parents)
             (list (kw->class kw meta)))) '() #:name name))
         (obj-name (string->symbol
                    (string-append
                     (symbol->string name) "-goops"))))
     (name-object goops)
     (set-object-property! goops 'name obj-name)
     (define! obj-name goops)
     goops))

(define (get-cparents supers)
  (let ((parents (filter-parents supers)))
    (if (null? parents)
        (if object
            (list object)
            '())
        parents)))

(define (get-mros supers)
  (get-mro2 (get-cparents supers)))

(define (get-h-as-dict self)
  (property-d
   (lambda (self)
     (dictRNs (slot-ref self 'h)))))

(define (add-specials pylist-set! dict name goops supers meta doc)
  (define (make-module)
    (let ((l (module-name (current-module))))
      (string-join
       (map symbol->string
            (if (and (>= (length l) 3)
                     (equal? (list-ref l 0) 'language)
                     (equal? (list-ref l 1) 'python)
                     (equal? (list-ref l 2) 'module))
                (cddr l)
                l))
       ".")))
    
  (define parents  (filter-parents supers))
  (define cparents (get-cparents supers))

  (define (filt-bases x)
    (let lp ((x x))
      (if (pair? x)
          (let ((y (car x)))
            (if (is-a? y <p>)
                (cons y (lp (cdr x)))
                (lp (cdr x))))
          '())))

  (pylist-set! dict '__goops__    goops)
  (pylist-set! dict '__zub_classes__ (make-weak-key-hash-table))
  (pylist-set! dict '__module__   (make-module))
  (pylist-set! dict '__bases__    (filt-bases parents))
  (pylist-set! dict '__name__     name)
  (pylist-set! dict '__qualname__ name)
  (pylist-set! dict '__mro__      (get-mro2 cparents))
  (pylist-set! dict '__doc__      (if doc doc ""))
  (pylist-set! dict '__class__    meta))

(define-method (scheme? x) #f)

(define boolObj (slask-it #t))

(define (type-1 meta obj)
  (catch #t
    (lambda ()
      (cond
       ((scheme? obj) =>
        (lambda (x) x))
       
       ((eq? obj 'None)
        NoneObj)

       ((eq? obj 'Ellipse)
        EllipseObj)
       
       (else
        (aif it (get-class obj)
             it
             (check-obj obj)))))
    (lambda x
      (warn x)
      (check-obj obj))))

(define (tr-parents x)
    (map (lambda (x)
           (aif it (hashq-ref classtranslator x #f)
                it
                x))
         (to-list x)))

(define (find-slots class)
  (define h (make-hash-table))
  (let lp ((mro (get-mro class)) (l '()))
    (if (pair? mro)
        (let lp2 ((ll (to-list (find-in-class-py (car mro) '__slots__ '())))
                  (l  l))
          (if (pair? ll)
              (let ((x (car ll)))
                (if (not (hash-ref h x))
                    (begin
                      (hash-set! h x #t)
                      (lp2 (cdr ll) (cons x l)))
                    (lp2 (cdr ll) l)))
              (lp (cdr mro) l)))
        (reverse
         (map (lambda (x) (if (string? x) (string->symbol x))) l)))))
         
(define (type-2 meta name parents dict kw)
  (set! parents (tr-parents parents))
  
  (set! name (if (symbol? name) name (string->symbol name)))

  
  (let* ((raw?    #f)
         (goops   (catch #t
                    (lambda () (pylist-ref dict '__goops__))
                    (lambda x
                      (set! raw? #t)
                      (get-goops2 meta name parents kw))))
         (p       (kwclass->class kw meta))
         (class   (make-p <pyc>)))

    (slot-set! class 'slots  (make-hash-table))
    (slot-set! class 'class meta)
    (slot-set! class 'mro   '())
    (slot-set! class 'goops goops)
    (slot-set! class 'procedure
      (lambda x
        (create-object class x)))
  
    (when class
      (let lp ((mro (catch #t
                      (lambda () (pylist-ref dict '__mro__))
                      (lambda x  (get-mros parents)))))
      
        (if (pair? mro)
            (let ((p (car mro)))
              (aif it (find-in-class-py p '__zub_classes__ #f)
                   (hash-set! it class #t)
                   #f)
	    
              (aif it (find-in-class-py p '__init_subclass__ #f)
                   (apply it class p #f kw)
                   #f)
              
              (lp (cdr mro)))))
      
      (hash-for-each*
       (lambda (k v)
         (let ((k (if (string? k) (string->symbol k) k)))
           (rawset class k v)))
       dict)
      
      (if raw?
          (begin
            (add-specials rawset class name goops parents meta
              (catch #t
                (lambda () (pylist-ref kw "doc"))
                (lambda x #f)))
            #;
            (set (rawref class '__module__)
                 (if (string? name) (string->symbol name) name)
                 class))
          (rawset class '__goops__ goops))
    
      (let ((mro (add-default class
                   (catch #t
                     (lambda () (pylist-ref dict '__mro__))
                     (lambda x (get-mros parents))))))
        (slot-set! class 'mro mro)
        (rawset class '__mro__ mro)
        (slot-set! class 'type
                   (aif it
                        (if (> (length mro) 1)
                            (logand 3 (get-type (car (reverse mro))))
                            2)
                        it
                        2)))


      (catch #t
        (lambda ()
          (if (not (ficap-raw class '__getattribute__ #f))
              (rawset class '__getattribute__ attr)))
        (lambda x
          (rawset class '__getattribute__ attr)))
      (slot-set! class 'ref    (ficap class '__getattribute__ #f))
      (slot-set! class 'set    (ficap class '__setattr__      #f))
      (slot-set! class 'init   (ficap class '__init__         #f))
      (slot-set! class 'new    (ficap class '__new__          #f))
      (slot-set! class 'eq     (ficap class '__eq__           #f))
      (slot-set! class 'hash   (ficap class '__hash__         #f))
      (slot-set! class 'bool   (ficap class '__bool__         #f))
      (slot-set! class 'geti   (ficap class '__getitem__      #f))
      (slot-set! class 'seti   (ficap class '__setitem__      #f))
      (slot-set! class 'cslots (find-slots class))
      (if meta
          (let ((h (get-slots class)))
            (for-each (lambda (x) (hash-set! h x 'None))
                      (get-cslots meta))
            '()))
      class)))

(define (new-class0 meta . l)
  (match l
    ((name parents dict . kw)
     (type-2 meta name (tr-parents parents) dict kw))
    
    ((obj)
     (type-1 meta obj))))


(define (new-class meta name parents dict kw)
  (aif it (and meta (get-new meta))
       (apply it meta name (tr-parents parents) dict kw)
       (apply new-class0 meta name parents dict kw)))

(define (type- meta name parents dict keys)
  (let ((class (new-class meta name (tr-parents parents) dict keys)))
    (aif it (and meta (get-init meta))
         (it class name parents dict keys)
         #f)
    
    class))


(define (the-create-object class x)
  (let* ((meta  (and class (get-class class)))
         (goops (get-goops class))
         (obj   (aif it (get-new class)
                     (apply it class x)
                     (make-object class meta goops))))

    (when (struct? obj)
      (aif it (get-init class)
	   (apply it obj x)
	   #f)

      (slot-set! obj 'procedure
		 (lambda x
		   (aif it (ficap class '__call__ #f)
			(apply it obj x)
			(error "not a callable object")))))
    
    obj))

(define (create-object class x)
  (if (pytype? class)
      (apply type-call class x)
      (let ((meta (and class (get-class class))))
	(with-fluids ((*make-class* #t))
	  (aif it (ficap meta '__call__ #f)
	       (apply it class x)
	       (the-create-object class x))))))

;; This are finished in the _python.scm module
(define int-cls    #f)
(define int?       #f)
(define tuple-cls  #f)
(define tuple?     #f)
(define string-cls #f)
(define str?       #f)
(define bytes-cls  #f)
(define bytes?     #f)
(define list-cls   #f)
(define list?      #f)
(define float-cls  #f)
(define float?     #f)

(define (check-obj obj)
  (cond
   ((int?    obj) int-cls)
   ((tuple?  obj) tuple-cls)
   ((float?  obj) float-cls)
   ((str?    obj) string-cls)
   ((list?   obj) list-cls)
   ((bytes?  obj) bytes-cls)
   (else
    object)))
                 
(define type-call
  (lambda (class . l)
    (if (pytype? class)
        (aif it (get-new class)
             (apply it class l)
             (apply (case-lambda
                      ((meta obj)
                       (type-1 class obj)
                       (catch #t
                         (lambda ()
                           (cond
                            ((eq? obj 'None)
                             NoneObj)
                            (else
                             (aif it (get-class obj)
                                  it
                                  (check-obj obj)))))
                         (lambda x
                           (warn x)
                           (check-obj obj))))
                  
                      ((meta name bases dict . keys)
                       (type- meta name bases dict keys)))
                    class l))
        (the-create-object class l))))

(define (get-dict self name parents)
  (aif it (and self (ficap self '__prepare__ #f))
       (dictNs (it self name parents))
       (make-hash-table)))

(define (create-class meta name parents gen-methods keys)
  (define parents2 (tr-parents parents))
  (let ((dict (gen-methods (get-dict meta name parents2))))
    (aif it (and meta (get-class meta))
         (aif it (ficap-raw it '__call__ #f)
              (apply it meta name parents2 dict keys)
              (type- meta name parents2 dict keys))
         (type- meta name parents2 dict keys))))

(define (make-object class meta goops)
  (let* ((ss  (map (lambda (x) (cons x 'None)) (get-cslots class)))
         (obj (if (null? ss) (make-p goops) (make-p0 goops))))
    (slot-set! obj 'class  class)
    (slot-set! obj 'type   0)
    (slot-set! obj 'mro   '())
    (slot-set! obj 'slots  ss)
    (if (null? ss)
        (rawset obj '__class__ class)
        (slot-set! obj 'slots (append ss (list (cons '__class__ class)))))
    obj))

;; Make an empty pf object
(define (make-p <x>)
  (let ((r (make <x>)))
    (cond
     ((is-a? r <pf>)
      (slot-set! r 'h vlist-null)
      (slot-set! r 'size 0)
      (slot-set! r 'n 0))
     ((is-a? r <p>)
      (slot-set! r 'h (make-hash-table)))
     (else
      (error "make-p in pf-objects need a <p> or <pf> derived class got ~a"
             r)))
    r))

(define (make-p0 <x>)
  (let ((r (make <x>)))
    (cond
     ((is-a? r <pf>)
      (slot-set! r 'h vlist-null)
      (slot-set! r 'size 0)
      (slot-set! r 'n 0))
     ((is-a? r <p>)
      (slot-set! r 'h #f))
     (else
      (error "make-p in pf-objects need a <p> or <pf> derived class got ~a"
             r)))
    r))

(define-syntax-rule (hif it (k h) x y)
  (let ((a (vhash-assq k h)))
    (if (pair? a)
        (let ((it (cdr a)))
          x)
        y)))

(define-syntax-rule (cif (it h) (k cl) x y)
  (let* ((h (get-h cl))
         (a (vhash-assq k h)))
    (if (pair? a)
        (let ((it (cdr a)))
          x)
        y)))

(define-inlinable (gox obj it)
  (let ((class (fluid-ref *location*)))
    (aif f (rawref it '__get__)
	 (f obj class)
	 it)))

(define-inlinable (gokx obj class it)
  (aif f (rawref it '__get__)
       (f obj class)
       it))

(define *location* (make-fluid #f))
(name-object *location*)

(define-syntax-rule (mrefx x key l)
  (let ()
    (define (end)
      (if (null? l)
          #f
          (car l)))

    (define (parents li)
      (let lp ((li li))
        (if (pair? li)
            (let ((p (car li)))
              (cif (it h) (key p)
                   (begin (fluid-set! *location* p) it)
		   (lp (cdr li))))
            fail)))
  
    (cif (it h) (key x)
         (begin (fluid-set! *location* x) it)
         (hif cl ('__class__ h)
              (cif (it h) (key cl)
                   (begin (fluid-set! *location* cl) it)
                   (hif p ('__mro__ h)
                        (let ((r (parents p)))
                          (if (eq? r fail)
                              (end)
                              r))
                        (end)))
              (end)))))
  
(define-syntax-rule (mrefx klass key l)
  (let ()
    (define (end) (if (pair? l) (car l) #f))    
    (fluid-set! *location* klass)
    (kif it (find-in-class-and-parents-p klass key fail)
	 it
	 (aif klass (and klass (find-in-class-p klass '__class__ #f))
	      (begin
		(fluid-set! *location* klass)
		(kif it (find-in-class-and-parents-p klass key fail)
		     it
		     (end)))
	      (end)))))

(define not-implemented (cons 'not 'implemeneted))


(define (mrefx-py xx key er)
  (define (exit) er)
  (kif it (let ((slots (get-slots xx)))
            (cond
             ((hash-table? slots)
              (hash-ref slots key fail))
             ((and (pair? slots) (pair? (car slots)))
              (aif it
                   (assq key (get-slots xx))
                   (cdr it)
                   fail))
             (else fail)))
       
       it
       (aif class (get-class xx)
            (aif f (get-ref class)
                 (kif it (if (eq? f __getattribute__)
                             (f xx key)
                             (catch #t
                               (lambda () (f xx (symbol->string key)))
                               (lambda x fail)))
                      it
                      (exit))
                 (kif it (__getattribute__ xx key)
                      it
                      (exit)))
            (exit))))

(define-syntax-rule (mref x key l)
  (let ((xx x))
    (mrefx xx key l)))

(define-syntax-rule (mref-py x key er)
  (let ((xx x))
    (let ((res (mrefx-py xx key er)))
      res)))

(define-method (ref x key . l)
  (cond
   ((eq? x 'None)
    (apply ref NoneObj key l))
   ((pair? l)
    (car l))
   (else
    #f)))

(define-syntax-rule (mox o x)
  (if (and (procedure? x) (not (is-a? x <p>)))
      (aif it (procedure-property- x '__get__)
           (it x o (fluid-ref *location*))
           x)))

(define-method (ref (x <pf> )  key . l) (mox x (mref     x key l)))
(define-method (ref (x <p>  )  key . l) (mox x (mref     x key l)))
(define-method (ref (x <pyf>)  key . l) (mref-py  x key
                                                  (if (pair? l) (car l) #f)))
(define-method (ref (x <py> )  key . l) (mref-py  x key
                                                  (if (pair? l) (car l) #f)))

(define-method (rawref x key . l) (if (pair? l) (car l) #f))
(define-method (rawref (x <pf> )  key . l) (mref     x key l))
(define-method (rawref (x <p>  )  key . l) (mref     x key l))

(define-method (rawdel (x <p>  )  key)
  (hash-remove! (get-h x)
                (if (symbol? key)
                    key
                    (string->symbol key))))

(define-method (set (f <procedure>) key val)
  (set-procedure-property!- f key val))

(define-method (ref (f <procedure>) key . l)
  (aif it (assoc key (procedure-properties- f))
       (cdr it)
       (if (pair? l) (car l) #f)))


;; the reshape function that will create a fresh new pf object with less size
;; this is an expensive operation and will only be done when we now there is
;; a lot to gain essentially tho complexity is as in the number of set
(define (reshape x)
  (let ((h (get-h x))
        (m (make-hash-table))
        (n 0))
    (define h2 (vhash-fold (lambda (k v s)
                             (if (hash-ref m k #f)
                                 s
                                 (begin
                                   (hash-set! m k #t)
                                   (set! n (+ n 1))
                                   (vhash-consq k v s))))
                           vlist-null
                           h))
    (slot-set! x 'h h2)
    (slot-set! x 'size n)
    (slot-set! x 'n    n)
    (values)))

;; on object x add a binding that key -> val
(define-method (mset (x <pf>) key val)
  (let ((h (get-h x))
        (s (slot-ref x 'size))
        (n (slot-ref x 'n)))
    (slot-set! x 'size (+ 1 s))
    (let ((r (vhash-assoc key h)))
      (when (not r)
        (slot-set! x 'n (+ n 1)))
      (slot-set! x 'h (vhash-cons key val h))
      (when (> s (* 2 n))
        (reshape x))
      (values))))
(name-object mset)

(define (pkh h) (hash-for-each (lambda x (pk x)) h) h)

(define-method (mset (x <p>) key val)
  (begin
    (hash-set! (get-h x) key val)
    (values)))

(define *make-class* (make-fluid #f))
(name-object *make-class*)

(define (mc?) (not (fluid-ref *make-class*)))

(define __setattr__
  (lambda (self key1 val)    
    (define key (if (string? key1) (string->symbol key1) key1))
    (define h   (aif it (get-h self)
                     it
                     (let ((h (make-hash-table)))
                       (slot-set! self 'h h)
                       h)))
    (if (not (frozen? self))
        (let ((f0 (aif dict (hash-ref h '__dict__)
                       (if (not (rawref dict '__raw__))
                           (lambda () (pylist-set! dict key val))
                           (lambda () (hash-set! h key val)))
                       (lambda () (hash-set! h key val)))))

          (let ((class (get-class self)))
            (kif find (hash-ref (get-slots class) #f)
                 (kif it (ficap class key fail)
                      (aif it2 (qref it '__set__ #f)
                           (it2 self val)
                           (f0))
                      (f0))
                 (f0))))
        (error "ValueError: Cannot mutate a readonly object" self))))

(define (mset-py x key val)
  (define (cont)
    (aif class (get-class x)
         (aif f (get-set class)
              (if (eq? f __setattr__)
                  (f            x key val)
                  (f            x (symbol->string key) val))
              (__setattr__  x key val))
         (mset x key val)))

  (let ((slots (get-slots x)))
    (cond
     ((and (pair? slots) (pair? (car slots)))
      (aif it (assq key slots)
           (set-cdr! it val)
           (cont)))
     ((null? slots)
      (cont))
     (else
      (kif found (hash-ref slots key fail)
           (hash-set! slots key val)
           (cont))))))

(define-syntax-rule (mklam (mset a ...) val)
  (mset a ... val))

(define-method (set (x <pf>)   key val) (mklam (mset     x key) val))
(define-method (set (x <p>)    key val) (mklam (mset     x key) val))
(define-method (set (x <pyf>)  key val) (mklam (mset-py  x key) val))
(define-method (set (x <py>)   key val) (mklam (mset-py  x key) val))
(define-method (set (x <pyc>)  key val) (mklam (mset-py  x key) val))

(define-method (rawset (x <pf>)  key val) (mklam (mset     x key) val))
(define-method (rawset (x <p>)   key val) (mklam (mset     x key) val))

;; mref will reference the value of the key in the object x, an extra default
;; parameter will tell what the fail object is else #f if fail
;; if there is no found binding in the object search the class and
;; the super classes for a binding

;; call a function as a value of key in x with the object otself as a first
;; parameter, this is pythonic object semantics
(define-syntax-rule (mk-call mcall mref)
  (define-syntax-rule (mcall x key l)
    (apply (mref x key '()) l)))

(mk-call mcall     mref)
(mk-call mcall-py  mref-py)
  
(define-method (call (x <pf>)  key . l) (mcall     x key l))
(define-method (call (x <p>)   key . l) (mcall     x key l))
(define-method (call (x <pyf>) key . l) (mcall-py  x key l))
(define-method (call (x <py>)  key . l) (mcall-py  x key l))
(name-object call)

;; make a copy of a pf object
(define-syntax-rule (mcopy x)
  (let ((r (make-p (ref x '__goops__))))
    (slot-set! r 'h (get-h x))
    (slot-set! r 'size (slot-ref x 'size))
    (slot-set! r 'n (slot-ref x 'n))
    r))

(define-syntax-rule (mcopy- x)
  (let* ((r (make-p (ref x '__goops__)))
         (h (get-h r)))
    (hash-for-each (lambda (k v) (hash-set! h k v)) (get-h x))
    r))

(define-method (copy (x <pf>)) (mcopy  x))
(define-method (copy (x <p> )) (mcopy- x))
(name-object copy)

;; make a copy of a pf object
(define-syntax-rule (mtr r x)
  (begin
    (slot-set! r 'h    (get-h x))
    (slot-set! r 'size (slot-ref x 'size))
    (slot-set! r 'n    (slot-ref x 'n   ))
    (values)))

(define-syntax-rule (mtr- r x)
  (begin
    (slot-set! r 'h (get-h x))
    (values)))
  

(define-method (tr (r <pf>) (x <pf>)) (mtr  r x))
(define-method (tr (r <p> ) (x <p> )) (mtr- r x))
(name-object tr)

;; with will execute thunk and restor x to it's initial state after it has
;; finished note that this is a cheap operatoin because we use a functional
;; datastructure
(define-syntax-rule (mwith x thunk)
  (let ((old (mcopy x)))
    (let ((r (thunk)))
      (slot-set! x 'h    (get-h old))
      (slot-set! x 'size (slot-ref old 'size))    
      (slot-set! x 'n    (slot-ref old 'n))
      r)))

(define-syntax-rule (mwith- x thunk)
  (let ((old (mcopy- x)))
    (let ((r (thunk)))
      (slot-set! x 'h    (get-h old))
      r)))



;; a functional set will return a new object with the added binding and keep
;; x untouched
(define-method (fset (x <pf>) key val)
  (let ((x (mcopy x)))
    (mset x key val val)
    x))
(name-object fset)

(define-method (fset (x <p>) key val)
  (let ((x (mcopy- x)))
    (mset x key val val)
    x))

(define (fset-x obj l val)
  (let lp ((obj obj) (l l) (r '()))
    (match l
      (()
       (let lp ((v val) (r r))
         (if (pair? r)
             (lp (fset (caar r) (cdar r) v) (cdr r))
             v)))
      ((k . l)
       (lp (ref obj k #f) l (cons (cons obj k) r))))))



           

;; a functional call will keep x untouched and return (values fknval newx)
;; e.g. we get both the value of the call and the new version of x with
;; perhaps new bindings added
(define-method (fcall (x <pf>) key . l)
  (let* ((y (mcopy x))
         (r (mcall y key l)))
    (if (eq? (get-h x) (get-h y))
        (values r x)
        (values r y))))
(name-object fcall)

(define-method (fcall (x <p>) key . l)
  (let ((x (mcopy x)))
    (values (mcall x key l)
            x)))

;; this shows how we can override addition in a pythonic way

;; lets define get put pcall etc so that we can refer to an object like
;; e.g. (put x.y.z 1) (pcall x.y 1)

(define-syntax-rule (cross x k f set)
  (call-with-values (lambda () f)
    (lambda (r y)
      (if (eq? x y)
          (values r x)
          (values r (set x k y))))))

(define-syntax-rule (cross! x k f _) f)

(define-syntax mku
  (syntax-rules ()
    ((_ cross set setx f (key) (val ...))
     (setx f key val ...))
    ((_ cross set setx f (k . l) val)
     (cross f k (mku cross set setx (ref f k) l val) set))))

(define-syntax-rule (mkk pset setx set cross)
  (define-syntax pset
    (lambda (x)   
      (syntax-case x ()
        ((_ f val (... ...))
         (let* ((to (lambda (x)
                      (datum->syntax #'f  (string->symbol x))))
                (l (string-split (symbol->string (syntax->datum #'f)) #\.)))
           (with-syntax (((a (... ...)) (map (lambda (x) #`'#,(to x))
                                             (cdr l)))
                         (h       (to (car l))))
             #'(mku cross setx set h (a (... ...)) (val (... ...))))))))))

(mkk put    fset  fset cross)
(mkk put!   set   set  cross!)
(mkk pcall! call  fset cross!)
(mkk pcall  fcall fset cross)
(mkk get    ref   fset cross!)

;; it's good to have a null object so we don't need to construct it all the
;; time because it is functional we can get away with this.
(define null (make-p <pf>))
           
(define (defaulter d)
  (if d
      (aif it (ref d '__goops__)
           it
           (if (is-a? d <py>)
               <py>
               <p>))
      <py>))

(define (kwclass->class kw default)
  (if (memq #:functionalClass kw)
      (if (memq #:fastClass kw)
          <pf>
          (if (memq #:pyClass kw)
              <pyf>
              (if (or (is-a? default <py>) (is-a? default <pyf>))
                  <pyf>
                  <pf>)))
      (if (memq #:mutatingClass kw)
          (if (memq #:fastClass kw)
              <p>
              (if (memq #:pyClass kw)
                  <py>
                  (if (or (is-a? default <py>) (is-a? default <pyf>))
                      <py>
                      <p>)))
          (if (memq #:fastClass kw)
              (if (or (is-a? default <pf>) (is-a? default <pyf>))
                  <pf>
                  <p>)
              (if (memq #:pyClass kw)
                  (if (or (is-a? default <pf>) (is-a? default <pyf>))
                      <pyf>
                      <py>)
                  (defaulter default))))))

(define type   #f)
(define object #f)

(define make-p-class
  (case-lambda
   ((name supers.kw methods)
    (make-p-class name "" supers.kw methods))
   ((name doc supers.kw methods)
    (define s.kw     supers.kw)
    (define kw       (cdr s.kw))
    (define supers   (tr-parents (car s.kw)))
    (define parents  (filter-parents supers))
    (define cparents (get-cparents supers))
    (define meta (aif it (memq #:metaclass kw)
		      (cadr it)
		      (if (null? cparents)
			  type
			  (let* ((p   (car cparents))
				 (m   (get-class p))
				 (mro (reverse (if m (get-mro m) '()))))
                            (let lp ((l (cdr cparents)) (m m) (mro mro))
                              (match l
                                ((pp . l)
                                 (aif mm (get-class pp)
                                      (aif mmro (if mm (get-mro mm) #f)
                                           (cond
                                            ((memq m mmro)
                                             (lp l mm mmro))
                                            ((memq mm mro)
                                             (lp l m  mro))
                                            (else
                                             (error "TypeError for meta")))
                                           (lp l m mro))
                                      (lp l m mro)))
                                (() m)))))))
                                  
    (define goops (get-goops2 meta name supers kw))
    
    (define (gen-methods dict)
      (methods dict)
      (add-specials pylist-set! dict name goops supers meta doc)
      dict)
    (let ((cl (with-fluids ((*make-class* #t))
                (create-class meta name parents gen-methods kw))))
      (aif it (if meta (ficap meta '__init_subclass__ #f) #f)
	   (let lp ((ps cparents))
	     (if (pair? ps)
		 (let ((super (car ps)))
		   (it cl super)
		   (lp (cdr ps)))))
	   #f)
      cl))))
		    


;; Let's make an object essentially just move a reference

;; the make class and defclass syntactic sugar

(define-syntax make-up
  (lambda (x)
    (syntax-case x ()
      ((_ (x . l))
      (member (syntax->datum #'x)
              '(lam lam0 lambda case-lambda lambda* letrec letrec*))
      #'(object-method (x . l)))
      ((_ x) #'x))))

(define-syntax mk-p-class
  (lambda (x)
    (syntax-case x ()
      ((_ name parents (ddef dname dval) ...)
       #'(mk-p-class name parents "" (ddef dname dval) ...))
      ((_ name parents doc (ddef dname dval) ...)
       (with-syntax (((ddname ...)
		      (map (lambda (dn)
			     (datum->syntax
			      #'name
			      (string->symbol
			       (string-append
				(symbol->string
				 (syntax->datum #'name))
				"-"
				(symbol->string
				 (syntax->datum dn))))))
			   #'(dname ...)))
		     (nname (datum->syntax
			     #'name
			     (string->symbol
			      (string-append
			       (symbol->string
				(syntax->datum #'name))
			       "-goops-class")))))
	 (%add-to-warn-list (syntax->datum #'nname))
         (map
          (lambda (x) (%add-to-warn-list (syntax->datum x)))
          #'(ddname ...))
         #'(let ()
	    (define name 
	      (letruc ((dname (make-up dval)) ...)
		      (let ((ret
			     (make-p-class 'name doc
					   parents
					   (lambda (dict)
					     (pylist-set! dict 'dname dname)
					     ...
					     (values)))))
			(begin
			  (module-define! (current-module) 'ddname dname)
			  (name-object ('ddname dname)))
			...
			ret)))
	    
	    (module-define! (current-module) 'nname (rawref name '__goops__))
	    (name-object ('nname (rawref name '__goops__)))
	    (name-object name)
	    name))))))

(define-syntax mk-py-class
  (lambda (x)
    (syntax-case x ()
      ((_ name parents (ddef dname dval) ...)
       #'(mk-p-class name parents "" (ddef dname dval) ...))
      ((_ name parents doc (ddef dname dval) ...)
       (with-syntax (((ddname ...)
		      (map (lambda (dn)
			     (datum->syntax
			      #'name
			      (string->symbol
			       (string-append
				(symbol->string
				 (syntax->datum #'name))
				"-"
				(symbol->string
				 (syntax->datum dn))))))
			   #'(dname ...)))
		     (nname (datum->syntax
			     #'name
			     (string->symbol
			      (string-append
			       (symbol->string
				(syntax->datum #'name))
			       "-goops-class")))))
	 (%add-to-warn-list (syntax->datum #'nname))
         (map (lambda (x) (%add-to-warn-list (syntax->datum x)))
              #'(ddname ...))
	#'(let ()
	    (define name 
	      (let ((dname (make-up dval)) ...)
                (let ((ret
                       (make-p-class 'name doc
                                     parents
                                     (lambda (dict)
                                       (pylist-set! dict 'dname dname)
                                       ...
                                       (values)))))
                  (begin
                    (module-define! (current-module) 'ddname dname)
                    (name-object ('ddname dname)))
                  ...
                  ret)))
	    
	    (module-define! (current-module) 'nname (rawref name '__goops__))
	    (name-object ('nname (rawref name '__goops__)))
	    (name-object name)
	    name))))))

(define (replace-stx ass code)
  (define m (make-hash-table))
  (define (p x)
    (pretty-print (syntax->datum x)) x)

  (define (tr0 x)
    (let lp ((x x))
      (syntax-case x (@@ @)
        ((#:identifier . a)
         #'(#:identifier . a))
        
        (((@ (guile) quotee) _)
         (equal? (syntax->datum #'quotee) 'quote)
         x)

        (((@@ a ref) x . y)
         (equal? (syntax->datum #'ref ) 'ref-x)
         #`((@@ a ref)
            #,(aif it (hash-ref m (syntax->datum #'x))
                   it
                   #'x)
            . #,(lp #'y)))
        
        (((@ a ref) x . y)
         (equal? (syntax->datum #'ref ) 'ref-x)
         #`((@ a ref)
            #,(aif it (hash-ref m (syntax->datum #'x))
                   it
                   #'x)
            . #,(lp #'y)))

        ((@ a b)
         x)
        ((@@ a n)
         x)

        ((a . b)
         (cons (lp #'a) (lp #'b)))
        (x
         (aif it (hash-ref m (syntax->datum #'x))
              it
              #'x)))))

  (define (tr x)
    (syntax-case x (@ @@)
      ((#:identifier . a)
       #'(#:identifier . a))
      (((@ (guile) quotee) _)
       (equal? (syntax->datum #'quotee) 'quote)
       x)
      ((@ a b)
       x)
      ((@@ a n)
       x)
      ((a . b)
       (cons (tr0 #'a) (tr #'b)))
      (x #'x)))

  (define (sec x)
    (syntax-case x (@ @@)
      ((#:identifier . a)
       #'(#:identifier . a))
      (((@ (guile) quotee) _)
       (equal? (syntax->datum #'quotee) 'quote)
       x)
      ((let ((a ((@@ b def) x y)) . l) . u)
       (equal? (syntax->datum #'def) 'def-decor)
       #`(let ((a ((@ b def) #,(tr #'x) y)) . l) . u))
      ((let ((a ((@ b def) x y)) . l) . u)
       (equal? (syntax->datum #'def) 'def-decor)
       #`(let ((a ((@ b def) #,(tr #'x) y)) . l) . u))
      (x (tr #'x))))
  
  (for-each
   (lambda (x)
     (hash-set! m (syntax->datum (car x)) (cdr x)))
   ass)

  (let lp ((code code))    
    (syntax-case code (@ @@)
      (((@@ a w) n1 n2 x)
       (eq? 'with-ni (syntax->datum #'w))
       #`((@@ a w) n1 n2 #,(lp #'x)))
      
      ((set! x y)
       (equal? (syntax->datum #'set!) 'set!)
       (aif it (hash-ref m (syntax->datum #'x))
            (if (syntax-case #'y (@@)
                  ((_ _ (_ _ ((@@ (_ _ _) mk) . _)))
                   (equal? (syntax->datum #'mk) 'mk-py-class2))
                  (_
                   #f))
                #`(set! #,it y)
                #`(set! #,it #,(sec #'y)))
            (error "wrong set! in classdef" (syntax->datum #'x))))

      (((@ (guile) set!) x y)
       (equal? (syntax->datum #'set!) 'set!)
       (aif it (hash-ref m (syntax->datum #'x))
            (if (syntax-case #'y (@@)
                  ((_ _ (_ _ ((@@ (_ _ _) mk) . _)))
                   (equal? (syntax->datum #'mk) 'mk-py-class2))
                  (_
                   #f))
                #`((@ (guile) set!) #,it y)
                #`((@ (guile) set!) #,it #,(sec #'y)))
            (error "wrong set! in classdef" (syntax->datum #'x))))

      (((@@ a set-x) x . y)
       (equal? (syntax->datum #'set-x) 'set-x)
       (aif it (hash-ref m (syntax->datum #'x))
            #`((@@ a set-x) #,it . #,(sec #'y))
            #`((@@ a set-x) x . #,(sec #'y))))
      
      (((@@ u qset!) x . y)
       (equal? (syntax->datum #'qset!) 'qset!)
       (aif it (hash-ref m (syntax->datum #'x))
            #`((@@ u qset!) #,it . #,(sec #'y))
            (error "wrong set! in classdef" (syntax->datum #'x))))

      (((@ a begin)
        ((@@ b qset!) c  d)
        e)
       (and (equal? (syntax->datum #'c)     (syntax->datum #'e))
            (equal? (syntax->datum #'begin) 'begin)
            (equal? (syntax->datum #'qset!)  'qset!))
       (aif it (hash-ref m (syntax->datum #'c))
            #`((@ a begin)
               ((@@ b qset!) #,it #,(sec #'d))
               #,it)
            (error "wrong set! in classdef" (syntax->datum #'x))))
           
      ((x . y)
       (cons (lp #'x) (lp #'y)))

      (x
       (if (symbol? (syntax->datum #'x))
           (aif it (hash-ref m (syntax->datum #'x))
                it
                #'x)
           #'x)))))

(define-syntax mk-p-class2
  (lambda (x)
    (syntax-case x ()
      ((_ name parents ((ddef dname dval) ...) body)
       #'(mk-p-class2 name parents "" ((ddef dname dval) ...) body))
      ((_ name parents doc ((ddef dname dval) ...) body)
       (with-syntax (((ddname ...)
		      (map (lambda (dn)
			     (datum->syntax
			      #'name
			      (string->symbol
			       (string-append
				(symbol->string
				 (syntax->datum #'name))
				"-"
				(symbol->string
				 (syntax->datum dn))))))
			   #'(dname ...)))
		     (nname (datum->syntax
			     #'name
			     (string->symbol
			      (string-append
			       (symbol->string
				(syntax->datum #'name))
			       "-goops-class")))))
         
         (%add-to-warn-list (syntax->datum #'nname))
         (map (lambda (x) (%add-to-warn-list (syntax->datum x)))
              #'(ddname ...))
         
         #'(let ()
             (define name
               (let ((pa parents))
                 (letruc2 ((dname (make-up dval)) ...)
                          body
                          (let ((ret
                                 (make-p-class 'name doc
                                               pa
                                               (lambda (dict)
                                                 (pylist-set! dict 'dname dname)
                                                 ...
                                                 dict))))
                            (begin
                              (module-define! (current-module) 'ddname dname)
                              (name-object ('ddname dname)))
                            ...
                            ret))))
             (module-define! (current-module) 'nname (rawref name '__goops__))
             (name-object ('nname (rawref name '__goops__)))
             (name-object name)
             name))))))

(define-syntax mk-py-class2
  (lambda (x)
    (syntax-case x ()
      ((_ name parents ((ddef dname dval) ...) body)
       #'(mk-p-class2 name parents "" ((ddef dname dval) ...) body))
      ((_ name parents doc ((ddef dname dval) ...) body)
       (with-syntax (((ddname ...)
		      (map (lambda (dn)
			     (datum->syntax
			      #'name
			      (string->symbol
			       (string-append
				(symbol->string
				 (syntax->datum #'name))
				"-"
				(symbol->string
				 (syntax->datum dn))))))
			   #'(dname ...)))
		     (nname (datum->syntax
			     #'name
			     (string->symbol
			      (string-append
			       (symbol->string
				(syntax->datum #'name))
			       "-goops-class")))))
         
         (%add-to-warn-list (syntax->datum #'nname))
         (map (lambda (x) (%add-to-warn-list (syntax->datum x)))
              #'(ddname ...))
         (let* ((dname2 (generate-temporaries #'(dname ...)))
                (body2  (replace-stx (map cons #'(dname ...) dname2) #'body)))
           (with-syntax (((dname2 ...) dname2)
                         (body2        body2))
             #'(let ()
                 (define name
                   (let ((pa parents))
                     (let ((dname2 (make-up dval)) ...)
                         body2
                         (let ((ret
                                (make-p-class 'name doc
                                              pa
                                              (lambda (dict)
                                                (pylist-set! dict 'dname dname2)
                                                ...
                                                dict))))
                           (begin
                             (module-define! (current-module) 'ddname dname2)
                             (name-object ('ddname dname2)))
                           ...
                           ret))))
                 
                 (module-define!
                  (current-module) 'nname (rawref name '__goops__))
                 (name-object ('nname (rawref name '__goops__)))
                 (name-object name)
                 name))))))))


(define-syntax mk-p-class-noname
  (lambda (x)
    (syntax-case x ()
      ((_ name parents (ddef dname dval) ...)
       #'(mk-p-class-noname name parents "" (ddef dname dval) ...))
      ((_ name parents doc (ddef dname dval) ...)
       #'(let ()
	   (define name 
	     (letruc ((dname dval) ...)
		     (make-p-class 'name doc
				   parents
				   (lambda (dict)
				     (pylist-set! dict 'dname dname)
				     ...
				     (values)))))
	   name)))))

(define-syntax-rule (def-p-class name . l)
  (define name (mk-p-class name . l)))

(define (get-type2 o)
  (cond
   ((is-a? o <pyf>)
    'pyf)
   ((is-a? o <py>)
    'py)
   ((is-a? o <pf>)
    'pf)
   ((is-a? o <p>)
    'p)
   (else
    'none)))

(define (print o l z)
  (begin
    (define p (if (pyclass? o) "C" (if (pyobject? o) "O" "T")))
    (define port (if (pair? l) (car l) #t))
    (format port "~a"
            (aif it (if (pyclass? o)
                        #f
                        (if (pyobject? o)
                            z
                            #f))
                 (if *pf-pretty*
                     (format
                      #f "~a" (it))
                     (format
                      #f "~a(~a)<~a>"
                      p (get-type2 o) (it)))

                 (if *pf-pretty*
                     (format
                      #f "<~a>"
                      (aif it (find-in-class-p
                               o '__name__ #f)
                           it
                           (ref
                            o '__name__ 'Annonymous)))
                     (format
                      #f "~a(~a)<~a>"
                      p (get-type2 o) (aif it (find-in-class-p
                                               o '__name__ #f)
                                           it
                                           (ref
                                            o '__name__ 'Annonymous))))))))

(define-method (write   (o <p>) . l)
  (aif it (ref o '__repr__)
       (print o l it)
       (print o l #f)))
(name-object write)

(define-method (display (o <p>) . l)
    (aif it (ref o '__repr__)
       (print o l it)
       (print o l #f)))
(name-object display)

(define (arglist->pkw l)
  (let lp ((l l) (r '()))
    (if (pair? l)
        (let ((x (car l)))
          (if (keyword? x)
              (cons (reverse r) l)
              (lp (cdr l) (cons x r))))
        (cons (reverse r) '()))))

(define-syntax-rule (define-python-class name (parents ...) code ...)
  (define name
    (syntax-parameterize ((*class* (lambda (x) #'name)))
       (mk-p-class name (arglist->pkw (list parents ...)) code ...))))

(define-syntax-rule (define-python-class-noname name (parents ...) code ...)
  (define name
    (syntax-parameterize ((*class* (lambda (x) #'name)))
      (mk-p-class-noname name (arglist->pkw (list parents ...))
			 code ...))))


(define-syntax make-python-class
  (lambda (x)
    (syntax-case x ()
      ((_ name (parents ...) code ...)
       #'(let* ((cl  (mk-p-class name
				 (arglist->pkw (list parents ...))
				 code ...)))
	   cl)))))

(define (py? x) (> (string-length
                    (symbol->string
                     (struct-layout x)))
                   10))

(define (pyobject? x)
  (and  (struct? x)
        (if (py? x)
            (eq? (logand 3 (get-type x)) 0)
            #t)))

(define (pyclass?  x)
  (and  (struct? x) (py? x) (eq? (logand 3 (get-type x)) 1)))

(define (pytype?   x)
  (and  (struct? x) (py? x) (eq? (logand 3 (get-type x)) 2)))

(define (frozen?   x)
  (and  (struct? x) (py? x)  (eq? (logand 4 (get-type x)) 4)))

(define (freeze    x) (slot-set! x 'type (logior 4 (get-type x))))

(define (mark-fkn tag f)
  (set-procedure-property!- f 'py-special tag)
  f)

(define-syntax-parameter
  *class* (lambda (x) (error "*class* not parameterized")))
(define-syntax-parameter
  *self* (lambda (x) (error "*class* not parameterized")))

(define *super* (list 'super))

(define (not-a-super) 'not-a-super)

(define (py-super class obj)
  (define (get)
    (if (= (logand 3 (get-type class)) (logand 3 (get-type obj)))
        obj
        (get-class obj)))
  
  (define (make cl parents)
    (if (not cl)
        #f
        (let ((c (make-p <pyc>))
	      (o (make-p <py>)))
          (let* ((pclass cl)
                 (mro    (cons c parents))
                 (att    (lambda (self key)
                           (__getattribute__ self key obj))))
            (slot-set! c 'slots '())
            (slot-set! c 'class (get-class pclass))
            (slot-set! c 'mro mro)
            (slot-set! c 'ref att)
            (slot-set! c 'type (get-type cl))            
            (rawset c '__class__        pclass)
            (rawset c '__getattribute__ att)
            (rawset c '__mro__ mro)        
            (rawset c '__name__  "**super**")
            (freeze c)
            
            (slot-set! o 'class c)
            (slot-set! o 'mro   (list o))
            (slot-set! o 'type  (max 0 (- (logand 3 (get-type obj)) 1)))
            (rawset o '__class__ c)
            (rawset o '__name__  "*super*")
            (freeze o)
            o))))
  
  (call-with-values
      (lambda ()
        (let ((ll (get-mro (get))))
          (if (pair? ll)
              (let lp ((l ll))
                (if (pair? l)
                    (if (eq? class (car l))
                        (let ((r (cdr l)))
                          (if (pair? r)
                              (values (car r) r)
                              (values #f      #f)))
                        (lp (cdr l)))
                    (values (car ll) ll)))
              (values #f #f))))
    
    make))

        
        
   
(define-syntax py-super-mac
  (syntax-rules ()
    ((_)
     (py-super *class* *self*))
    ((_ class self)
     (py-super class self))))

(define (pp x)
  (pretty-print (syntax->datum x))
  x)

(define-syntax letruc
  (lambda (x)
    (syntax-case x ()
      ((_ ((x v) ...) code ...)
       (let lp ((a #'(x ...)) (b #'(v ...)) (u '()))
         (if (pair? a)
             (let* ((x (car a))
                    (s (syntax->datum x)))
               (let lp2 ((a2 (cdr a)) (b2 (cdr b)) (a3 '()) (b3 '())
                         (r (list (car b))))
                 (if (pair? a2)
                     (if (eq? (syntax->datum a2) s)
                         (lp2 (cdr a2) (cdr b2) a3 b3 (cons (car b2) r))
                         (lp2 (cdr a2) (cdr b2)
                              (cons (car a2) a3)
                              (cons (car b2) b3)
                              r))
                     (lp (reverse a3) (reverse b3)
                         (cons
                          (list x #`(let* #,(map (lambda (v) (list x v))
                                                 (reverse r)) #,x))
                          u)))))
             #`(letrec #,(reverse u) code ...)))))))

(define-syntax letruc2
  (lambda (x)
    (syntax-case x ()
      ((_ ((x v) ...) code ...)
       (let lp ((a #'(x ...)) (b #'(v ...)) (u '()))
         (if (pair? a)
             (let* ((x (car a))
                    (s (syntax->datum x)))
               (let lp2 ((a2 (cdr a)) (b2 (cdr b)) (a3 '()) (b3 '())
                         (r (list (car b))))
                 (if (pair? a2)
                     (if (eq? (syntax->datum a2) s)
                         (lp2 (cdr a2) (cdr b2) a3 b3 (cons (car b2) r))
                         (lp2 (cdr a2) (cdr b2)
                              (cons (car a2) a3)
                              (cons (car b2) b3)
                              r))
                     (lp (reverse a3) (reverse b3)
                         (cons
                          (list x #`(let* #,(map (lambda (v) (list x v))
                                                 (reverse r)) #,x))
                          u)))))
             #`(let #,u code ...)))))))
             
(define-method (py-init . l)
  (values))
(name-object py-init)

(define-method (py-init (o <p>) . l)
  (aif it (ref o '__init__ #f)
       (apply it l)
       (next-method)))

(define (get-mro2 parents)
  (define h (make-hash-table))
  (let ((mros (map (lambda (x) (get-mro x)) parents)))
    (let lp1 ((mros mros) (i 0))
      (if (pair? mros)
          (let lp2 ((mro (car mros)) (i i))
            (if (pair? mro)
                (let ((m (car mro)))
                  (hashq-set! h m i)
                  (lp2 (cdr mro) (+ i 1)))
                (lp1 (cdr mros) i)))
          (let ((l (hash-fold (lambda (k v s) (cons (cons k v) s)) '() h)))
            (map car (sort l (lambda (x y) (< (cdr x) (cdr y))))))))))

(define-method (py-equal? (x <p>)  y)
  (aif it (ref x '__eq__)
       (it y)
       (next-method)))

(define-method (py-equal? (x <py>)  y)
  (aif it (get-eq (get-class x))
       (it x y)
       (next-method)))

(name-object py-equal?)

(define-method (py-equal? y (x <p>))
  (aif it (ref x '__eq__)
       (it y)
       (next-method)))

(define-method (py-equal? y (x <py>))
  (aif it (get-eq (get-class x))
       (it x y)
       (next-method)))

(define-method (py-equal? x y) ((@ (guile) equal?) x y))

(define (equal? x y)
  (or (eq? x y)
      (cond
       ((and (number?  x) (number? y))
        (= x y))
       ((and (string?  x) (string? y))
        ((@ (guile) equal?) x y))
       ((and (boolean? x) (boolean? y))
        (eq? x y))
       (else
        (py-equal? x y)))))

(define (subclasses self)
  (aif it (ref self '__zubclasses__)
       (let ((h (make-hash-table)))
	 (let lp0 ((it it))
	   (let lp ((l (hash-fold
			(lambda (k v s)
			  (hash-set! h k #t)
			  (cons k s))
			'() it)))
	     (if (pair? l)
		 (begin
		   (lp0 (car l))
		   (lp (cdr l))))))

	 (hash-fold (lambda (k v s) (cons k s)) '() h))
       '()))

(define ref-class
  (lambda (self key fail)
    (aif class (get-class self)
         (kif it1 (ficap class key fail)
              (aif dd1 (rawref it1 '__get__)
                   (dd1 self class)
                   it1)
              fail)
         fail)))

#|
If we first finds a sata descripto in the class (both get and set)
Then return it. 
Else look in the object
Else look in the class
Else try __getattr__
|#
(define lam-fail (lambda () fail))

(define __getattribute__
  (case-lambda
    ((a b)
     (__getattribute__ a b a))
    ((self b c)
     (if (pyobject? self)
         (getattribute-instance self b c)
         (getattribute-class    self b c)))))


(define (qref a b c)
  (cond
   ((struct? a)
    (if (py? a)
        (pyref a b c))
        (ref a b c))
    

   ((procedure? a)
    (aif it (procedure-property- a b)
         it
         c))   
   (else
    c)))

(define (getattribute-class self key- obj)    
  (define key (if (string? key-) (string->symbol key-) key-))
  
  (define (ff key it dd a b)
    (if (eq? key '__new__)
        it
        (dd a b)))
  
  (define (f0 it)
    (if (or (procedure? it) (struct? it))
        (aif d (qref it '__get__ #f)
             (ff key it d 'None obj)
             it)
        it))

  (define (f1 it meta)
    (hash-set! (get-slots meta) key fail)
    (f0 it))

  (kif it (ficap self key fail)
       (let ((meta (get-class self)))
         (kif found (hash-ref (get-slots meta) key)
              (kif it2 (ficap meta key fail)
                   (if (not (struct? it2))                       
                       (f1 it meta)
                       (aif found (qref it '__set__ #f)
                            (aif d (qref it '__get__ #f)
                                 (d obj meta)
                                 (f1 it meta))
                            (f1 it meta)))
                   (f1 it meta))
              it))
                    
       (let ((meta (get-class self)))
         (kif it (ficap meta key fail)
              (if (or (procedure? it) (struct? it))
                  (aif d (qref it '__get__ #f)
                       (ff key it d obj meta)
                       it)
                  it)
              (aif get (ficap meta '__getattr__ #f)
                   (catch #t
                     (lambda ()
                       (get obj (symbol->string key)))
                     (lambda x
                       fail))
                   fail)))))

(define (getattribute-instance self key- obj)    
  (define key (if (string? key-) (string->symbol key-) key-))

  (define (ff key it dd a b)
    (if (eq? key '__new__)
        it
        (dd key)))


  (define (f1 it class)
    (hash-set! (get-slots class) key fail)
    it)

  (kif it (if (get-h self) (find-in-class self key fail) fail)
       (let ((class (get-class self)))
         (kif found (hash-ref (get-slots class) key)
              (kif it2 (ficap class key fail)
                   (if (not (struct? it2))
                       (f1 it class)
                       (aif found (qref it '__set__ #f)
                            (aif it (qref it '__get__ #f)
                                 (it obj class)
                                 (f1 it class))
                            (f1 it class)))
                   (f1 it class))           
              it))
                    
       (let ((class (get-class self)))
         (kif it (ficap class key fail)
              (aif it2 (qref it '__get__ #f)
                   (it2 obj class)
                   it)
              (aif get (ficap class '__getattr__ #f)
                   (catch #t
                     (lambda ()
                       (get obj (symbol->string key)))
                     (lambda x
                       fail))
                   fail)))))


          
(define attr __getattribute__)

(define (*str* self . l)
  (scmstr (ref self '__name__)))

(define *setattr* __setattr__)

(define subclasscheck
  (lambda (self c)
    (aif it (rawref c '__mro__ #f)
         (memq self it)
         #f)))

(define (instancecheck cl o)
  (let lp ((l (get-mro (get-class o))))
    (if (pair? l)
        (or
         (eq? cl (car l))
         (lp (cdr l)))
        #f)))

(set! type
  (make-python-class type ()
     (define __new__           (object-method new-class0))
     (define __init_subclass__ (lambda x (values)))
     (define __zub_classes__   (make-weak-key-hash-table))
     (define __subclasses__    subclasses)
     (define __subclasscheck__ (object-method subclasscheck))
     (define __instancecheck__ (object-method instancecheck))
     (define __call__          (object-method type-call))
     (define __str__           (object-method *str*))
     (define __bool__          (object-method (lambda (x) x)))
     (define __hash__          (object-method
                                (lambda (self)
                                  (object-address self))))
     (define __eq__            (lambda (self x) (eq? self x)))
     (define __getattribute__  attr)
     (define __setattr__       (object-method *setattr*))
     (define __format__        (lambda (self x) (*str* self)))
     (define __reduce_ex__     (lambda x (error "not implemented")))
     (define mro               (begin
                                 (object-method
                                  (lambda (self) (ref self '__mro__)))))))

(rawset type '__class__ type)
(rawset type '__mro__   (list type))
(slot-set! type 'type   2)
(slot-set! type 'mro    (list type))
(slot-set! type 'class  type)
(slot-set! type 'ref    attr)
(slot-set! type 'set    *setattr*)
(slot-set! type 'new    new-class0)
(slot-set! type 'init   #f)

(define _mro (object-method (lambda (self) (ref self '__mro__))))

(define (scmstr s) (if (symbol? s) (symbol->string s) s))

(define (dirit x) (_dirit x))

(set! object
  (make-python-class object ()
    (define __new__          (lambda (class . a)
                               (let ((obj (make-object
                                           class
                                           (get-class class)
                                           (get-goops class))))
                                 (slot-set! obj 'class class)
                                 (slot-set! obj 'type  0)
                                 obj)))
    (define __init__         (lambda x (values)))
    (define __instancecheck__ (object-method instancecheck))
    (define __subclasses__   subclasses)
    (define __getattribute__ attr)
    (define __setattr__      (object-method *setattr*))
    (define __str__          *str*)
    (define __dir__          (object-method dirit))
    (define __hash__         (object-method
                              (lambda (self)
                                (object-address self))))
    (define __bool__         (object-method (lambda (self) self)))
    (define __ne__           (object-method
                              (lambda (self x) (not (equal? self x)))))
    (define __eq__            (lambda (self x) (eq? self x)))
    (define __format__        (lambda (self x) (*str* self)))
    (define __reduce_ex__     (lambda x (error "not implemented")))
    (define __weakref__      (lambda (self) self))))

(slot-set! object 'type  1)
(slot-set! object 'class type)
(slot-set! object 'mro   (list object))
(slot-set! object 'ref  attr)
(slot-set! object 'set  *setattr*)

(rawset object '__mro__   (list object))
(rawset object '__class__ type)
(name-object type)
(name-object object)
 
(define-method (py-class (o <p>))
  (aif it (ref o '__class__)
       it
       (next-method)))
(name-object py-class)

(define-method (py-class (o <symbol>))
  (cond
   ((eq? o 'None)
    NoneObj)))
  
(define-python-class NoneObj ()
  (define __new__
    (lambda x 'None)))
(set NoneObj '__name__ 'None)
(freeze NoneObj)

(define-python-class EllipseObj ()
  (define __new__
    (lambda x 'None)))
(set EllipseObj '__name__ 'Ellipse)
(freeze EllipseObj)

(define-method (py-dict x)
  (if (eq? x 'None)
      (py-dict NoneObj)
      (make-hash-table)))
(name-object py-dict)

(define-method (py-dict (o <p>))
  (aif it (find-in-class-py (get-class o) '__dict__ #f)
       it
       (dictRNs (get-h o))))

(define-syntax ref-in-class
  (syntax-rules ()
    ((_ x key  ) (ref-in-class x key #f))
    ((_ x key f)
     (begin
       (ficap (get-class x) key f)))))

(define (pyref a b c)
  (if (is-a? a <py>)
      (mrefx-py a b c)
      (ref a b c)))

(define (pyset a b c)
  (if (struct? a)
      (mset-py a b c)
      (set a b c)))

(define find-in-class find-in-class-py)
