(define-module (util scheme-python oop dict)
  #:replace (format)
  #:export (procedure-property- procedure-properties- set-procedure-property!-
                                set-procedure-properties!- dictNs dictRNs
                                hash-for-each* object-method class-method
                                static-method Module splitm splitmm
                                classtranslator _dirit def-dict
                                formatter m? bytes b? b-decode hset! slask-tuple
                                slask-procedure
                                hashforeach mkw to-list int slask-it leveler))


(define slask-tuple     1)
(define slask-procedure 1)
(define classtranslator (make-hash-table))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define (leveler x) (x))

(define _dirit (lambda (x) '()))

(define prophash (make-hash-table))

(define (procedure-property- o key . l)
  (define ret (if (pair? l) (car l) #f))    
  (aif props (hashq-ref prophash o)
       (aif it (assq key props)
            (cdr it)
            ret)
       ret))

(define (procedure-properties- o)
  (define ret '())
  (aif props (hashq-ref prophash o)
       props
       ret))

(define (set-procedure-property!- o key v)
  (hashq-set! prophash
              o
              (aif props (hashq-ref prophash o)
                   (cons (cons key v) props)
                   (list (cons key v)))))

(define (set-procedure-properties!- o l)
  (hashq-set! prophash o l))

;; this is mutated by the dict class
(define dictNs  (list 'dictNs))
(define dictRNs (list 'dictRNs))

;; this is mutated by the dict class
(define hash-for-each* hash-for-each)

(define (class-method f)
  f)

(define (object-method f)
  f)

(define (static-method f)
  f)

(define (Module x . l) (reverse x))

(define splitm  #f)
(define splitmm #f)

(define formatter #f)
(define format (lambda (a b) a))

(define (m? x) #f)
(define bytes #f)
(define b?       #f)
(define b-decode #f)
(define hset! hash-set!)

(define (hashforeach a b) (values))

(define (mkw kw) (error "not implemented"))

(define to-list (lambda (x) x))

(define (int x) x)

(define (slask-it x) x)

(define def-dict #f)
