(define-module (util scheme-python code)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (system vm program)
  #:use-module (util scheme-python property)
  #:use-module (ice-9 match)
  #:export (Code))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-python-class Code ()
"
co_argcount    number of arguments (not including keyword only arguments,
                                  * or ** args)
co_code        string of raw compiled bytecode
co_cellvars    tuple of names of cell variables 
                  (referenced by containing scopes)
co_consts      tuple of constants used in the bytecode
co_filename    name of file in which this code object was created
co_firstlineno number of first line in Python source code
co_flags       bitmap of CO_* flags, read more here
co_lnotab      encoded mapping of line numbers to bytecode indices
co_freevars    tuple of names of free variables 
                   (referenced via a function’s closure)
co_posonlyargcount  number of positional only arguments
co_kwonlyargcount   number of keyword only arguments (not including ** arg)
co_name             name with which this code object was defined
co_names            tuple of names of local variables
co_nlocals    number of local variables
co_stacksize  virtual machine stack space required
co_varnames   tuple of names of arguments and local variables
"
  
  (define __init__
    (lambda (self f)
      (set self '_f f)))

  (define _get_co_argcount
    (lambda (self)
      (let ((f (ref self '_f)))
        (aif it (ref f 'arglist)
             (let lp ((l it) (n 0))
               (match l
                 ((('* _) . _)
                  n)
                 ((('** _) . _)
                  n)
                 (()
                  n)
                 ((_ . l)
                  (lp l (+ n 1)))))
             ((@@ (system vm program) program-minimum-arity) f)))))
  
  (define co_argcount (property _get_co_argcount))

  (define co_code "")

  (define co_cellvars '())

  (define co_consts   '())

  (define co_filename
    (property
     (lambda (self)
       (aif it (ref (ref self '_f) '__file__)
            it
            #f))))
    
  (define co_firstlineno
    (property
     (lambda (self)
       (ref (ref self '_f) 'line))))
  
  (define co_flags 1) 

  (define co_freevars '())

  (define co_posonlyargcount
    (property
     (lambda (self)
       (let ((f (ref self '_f)))
         (aif it (ref f 'arglist)
              (let lp ((l it) (n 0))
                (match l
                  ((('* _) . _)
                   n)
                  ((('** _) . _)
                   n)
                  ((('= _ _) . _)
                   n)
                  (()
                   n)
                  ((_ . l)
                   (lp l (+ n 1)))))
              ((@@ (system vm program program-minimum-arity) f)))))))
    
  (define co_kwonlyargcount
    (property
     (lambda (self)
       (let ((f (ref self '_f)))
         (aif it (ref f 'arglist)
              (let lp ((l it) (n 0))
                (match l
                  ((('* _) . _)
                   n)
                  ((('** _) . _)
                   n)
                  ((('= _ _) . l)
                   (lp l (+ n 1)))
                  (()
                   n)
                  ((_ . l)
                   (lp l n))))
              0)))))
  
    
  (define co_lnotab 0)
  
  (define co_name
    (property
     (lambda (self)
       (symbol->string (procedure-name (ref self '_f))))))
    
  (define co_nlocals 0)

  (define co_stacksize 0)

  (define co_varnames
    (property
     (lambda (self) 
       (ref (ref self '_f) 'arglist)))))



    
