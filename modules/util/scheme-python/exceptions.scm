(define-module (util scheme-python exceptions)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (oop goops)
  #:export (StopIteration GeneratorExit RuntimeError TabError
                          Exception ValueError TypeError
                          IndexError KeyError AttributeError ArgumentError
                          SyntaxError SystemException
                          OSError ProcessLookupError PermissionError
                          None Ellipsis NotImplemented NotImplementedError
			  AssertionError ImportError
                          ModuleNotFoundError BlockingIOError
                          InterruptedError BaseException
			  ZeroDivisionError PendingDeprecationWarning
                          ImportWarning
			  OverflowError RecursionError RuntimeWarning
			  Warning DeprecationWarning BytesWarning
                          ResourceWarning UserWarning UnicodeTranslateError
                          UnicodeDecodeError LookupError IndentationError
                          KeyboardInterrupt MemoryError NameError
                          EOFError UnicodeError UnicodeEncodeError
                          FileExistsError FileNotFoundError IsADirectoryError
			  EnvironmentError ConnectionError NotADirectoryError
                          ConnectionResetError ChildProcessError TimeOutError
                          BrokenPipeError ConnectionAbortedError SystemExit
                          ConnectionRefusedError ArithmeticError 
                          FutureWarnng SyntaxWarning SCMError))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-python-class BaseException ()
  (define __init__
    (case-lambda
      ((self)
       (values))
      ((self val . l)
       (catch #t
         (lambda ()
           (set self 'value (apply format #f val l)))
         (lambda x
           (set self 'value val))))))

  (define __f
    (lambda (self)
      (aif it (rawref self 'value #f)
           (format #f "~a" it)
           (format #f "~a" (rawref self '__name__)))))

  (define __str__ __f)
  
  (define __repr__
    (lambda (self)
      (aif it (ref self '__str__)
           (it)
           (__f self)))))

(define-python-class SystemExit ()
  (define __init__
    (case-lambda
      ((self)
       (values))
      ((self val . l)
       (catch #t
         (lambda ()
           (set self 'value (apply format #f val l)))
         (lambda x
           (set self 'value val))))))
                 
  (define __repr__
    (lambda (self)
      (aif it (rawref self 'value #f)
           (format #f "~a"  it)
           (format #f "~a"
                   (rawref self '__name__))))))

(define-python-class Warning ()
  (define __init__
    (case-lambda
      ((self)
       (values))
      ((self val . l)
       (set self 'value val))))
                 
  (define __repr__
    (lambda (self)
      (aif it (rawref self 'value #f)
           (format #f "~a" it)
           (format #f "~a"
                   (rawref self '__name__))))))

(define-syntax define-er2
  (syntax-rules ()
    ((_ nm k)
     (define-python-class nm (BaseException)))
    ((_ nm w k)
     (define-python-class nm w))))

(define-er2 Exception 'Exception)

(define-python-class SyntaxWarning (Exception))

(define-syntax define-er
  (syntax-rules ()
    ((_ nm k)
     (define-python-class nm (Exception)))
    ((_ nm w k)
     (define-python-class nm w))))


(define-er StopIteration        'StopIteration)
(define GeneratorExit           'GeneratorExit)
(define-er SCMError             'SCMError)
(define-er EnvironmentError     'EnvironmentError)

(define-er EOFError             'EOFError)
(define-er MemoryError          'MemoryError)
(define-er NameError            'NameError)

(define-er ValueError           'ValueError)
(define-python-class UnicodeError (ValueError))
(define-python-class UnicodeDecodeError (UnicodeError))
(define-python-class UnicodeEncodeError (UnicodeError))
(define-python-class UnicodeTranslateError (UnicodeError))

(define-er LookupError          'LookupError)
  (define-python-class IndexError (LookupError))
  (define-python-class KeyError   (LookupError))

(define-er ArithmeticError        'OverflowError)
  (define-python-class OverflowError     (ArithmeticError))
  (define-python-class ZeroDivisionError (ArithmeticError))


(define-er2 KeyboardInterrupt    'KeyboardInterrupt)
(define-er SystemException      'SystemException)
(define-er RuntimeError         'RuntimeError)
  (define-python-class NotImplementedError (RuntimeError))
  (define-python-class RecursionError      (RuntimeError)) 


(define-er ArgumentError        'IndexError)

(define-er OSError              'OSError)
  (define-python-class BlockingIOError    (OSError))
  (define-python-class ChildProcessError  (OSError))
  (define-python-class ConnectionError    (OSError))
    (define-python-class BrokenPipeError        (ConnectionError))
    (define-python-class ConnectionAbortedError (ConnectionError))
    (define-python-class ConnectionRefusedError (ConnectionError))
    (define-python-class ConnectionResetError   (ConnectionError))
  (define-python-class FileExistsError    (OSError))
  (define-python-class FileNotFoundError  (OSError))
  (define-python-class InterruptedError   (OSError))
  (define-python-class IsADirectoryError  (OSError))
  (define-python-class NotADirectoryError (OSError))
  (define-python-class PermissionError    (OSError))
  (define-python-class ProcessLookupError (OSError))
  (define-python-class TimeOutError       (OSError))
(define None                    'None)
(define Ellipsis                'Ellipsis)


(define-er TypeError            'TypeError)

(define-python-class AttributeError (Exception)
  (define __init__
    (case-lambda
      ((self s x y)
       (if (and (struct? x) (is-a? x <p>))
           (let ((nm (ref (get-class x) '__name__)))
             (cond
              ((or (equal? nm 'Module) (equal? nm 'ModuleT))
               (set self 'value
                    (format #f "module '~a' has no attribute '~a'"
                            (rawref x '__name__) y)))
              (else
               ((rawref Exception '__init__) self x y))))
           ((rawref Exception '__init__) self x y)))
      (x (apply (rawref Exception '__init__) x)))))
       
(define-er SyntaxError          'SyntaxError)
 (define-python-class IndentationError (SyntaxError))
   (define-python-class TabError (IndentationError))

(define-er RunTimeError         'RunTimeError)

(define-er AssertionError       'AssertionError)
(define-er ImportError          'ImportError)      
(define-er ModuleNotFoundError  (ImportError) 'ModuleNotFoundError)

(define NotImplemented (list 'NotImplemented))


            

(define-syntax define-wr
  (syntax-rules ()
    ((_ nm k)
     (define-python-class nm (Warning)))
    ((_ nm w k)
     (define-python-class nm w))))

(define-wr BytesWarning              'BytesWarning)
(define-wr DeprecationWarning        'DeprecationWarning)
(define-wr ResourceWarning           'ResourceWarning)
(define-wr UserWarning               'UserWarning)
(define-wr PendingDeprecationWarning 'PendingDeprecationWarning)
(define-wr RuntimeWarning            'RuntimeWarning)
(define-wr FutureWarning             'FutureWarning)
(define-wr ImportWarning             'ImportWarning)

