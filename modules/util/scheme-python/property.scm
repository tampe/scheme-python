(define-module (util scheme-python property)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (oop goops)
  #:use-module (util scheme-python def)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python persist)
  #:export (property))

(cpit <property> (o (lambda (o get set del)
		      (slot-set! o 'get get)
		      (slot-set! o 'set set)
		      (slot-set! o 'del del))
		    (list
		     (slot-ref o 'get)
		     (slot-ref o 'set)
		     (slot-ref o 'del))))
		     
(define-python-class property (object <property>)
  (define __init__
    (lam (o (= getx None) (= setx None) (= delx None))
	 (slot-set! o 'get getx)
	 (slot-set! o 'set setx)
	 (slot-set! o 'del delx)
	 o))

  (define __get__
    (lambda (self obj class)
      ((slot-ref self 'get) obj)))

  (define __set__
    (lambda (self obj value)
      ((slot-ref self 'set) obj value)))
      
  (define setter
    (lambda (self f)
      (slot-set! self 'set f)
      self))

  (define getter
    (lambda (self f)
      (slot-set! self 'get f)
      self))

  (define deleter
    (lambda (self f)
      (slot-set! self 'del f)
      self))

  (define fget (lambda (self) (slot-ref self 'get)))
  (define fset (lambda (self) (slot-ref self 'set)))
  (define fdel (lambda (self) (slot-ref self 'del))))

