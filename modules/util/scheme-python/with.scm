(define-module (util scheme-python with)
  #:use-module (util scheme-python try)
  #:use-module (util scheme-python yield)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python persist)
  #:use-module (util scheme-python bool)
  #:use-module (util scheme-python for)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:replace (exit)
  #:export (with enter))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-method (enter o)
  (raise TypeError "no __enter__ member"))
(define-method (enter (o <yield>))
  (next o))
  
(define-method (enter (o <p>))
  (aif it (ref o '__enter__)
       (it)
       (next-method)))

(define-method (exit . l)
  (raise TypeError "no __exit__ member" l))

(define-method (exit (o <p>) . l)
  (aif it (ref o '__exit__)
       (apply it l)
       (next-method)))

(name-object enter)
(name-object exit)

(define-syntax with
  (syntax-rules ()
    ((_ () . code)
     (begin . code))
    ((_ (x . l) . code)
     (with0 x (with l . code)))))


(define-syntax with0
  (syntax-rules ()
    ((_ ((id ...) exp) . code)
     (let ((type   None)
           (value  None)
           (trace  None))
       (let ((x       #f)
             (e       #f)
             (mute    #f))
         (catch #t
           (lambda ()
             (try
              (lambda ()
                (set! e exp)
                (set! x (enter e))
                (match x
                  ((id ...)
                   . code)))                
              (#:except #t =>
                (lambda (tag l)
                  (set! type  (car   l))
                  (set! value (cadr  l))
                  (set! trace (caddr l))
                  (apply throw tag l)))
              
              #:finally
              (lambda ()
                (call-with-values
                    (lambda ()                      
                      (exit e type value trace))                                
                  (case-lambda
                    ((x . l)
                     (set! mute (bool x)))
                    (()
                     (set! mute #f)))))))
           
           (lambda x
             (when (not mute)
               (apply throw x)))))))

    ((_ (id exp) . code)
     (let ((type   None)
	   (value  None)
	   (trace  None))
       (let ((id      #f)
             (e       #f)
             (mute    #f))
         (catch #t
           (lambda ()
             (try
              (lambda ()
                (set! e  exp)
                (set! id (enter e))
                (begin . code))
              
              (#:except #t =>
                (lambda (tag l)
                  (set! type  (car   l))
                  (set! value (cadr  l))
                  (set! trace (caddr l))
                  (apply throw tag l)))

              #:finally
              (lambda ()
                (call-with-values
                    (lambda ()
                      (exit e type value trace))
                  (case-lambda
                    ((x . l)
                     (set! mute (bool x)))
                    (()
                     (set! mute #f)))))))
           
           (lambda x
             (when (not mute)
               (apply throw x)))))))

       ((_ (exp) . code)
        (with0 (id exp) . code))))

  
