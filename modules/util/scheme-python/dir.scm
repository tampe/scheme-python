(define-module (util scheme-python dir)
  #:use-module (util scheme-python list)
  #:use-module (util scheme-python for)
  #:use-module (util scheme-python dict)
  #:use-module (util scheme-python string)
  #:use-module (util scheme-python bytes)
  #:use-module (util scheme-python number)
  #:use-module (util scheme-python bytes)
  #:use-module (oop goops)
  #:use-module (ice-9 vlist)
  #:use-module (util scheme-python oop pf-objects)
  #:export (dir))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define in-p (make-fluid #f))
(define-method (dir x) (py-list))
(define (cont l1 l2)
  (let ((h  (make-hash-table))
        (l  (py-list)))
    (for ((x : l1)) ()
         (hash-set! h x #t))
    (for ((x : l2)) ()
         (hash-set! h x #t))
    (for ((k v : h)) ()
         (pylist-append! l k))
    (pylist-sort! l)
    
    l))

(define (p x) (if (symbol? x) (symbol->string x) x))
(define (chash-for-each t c)
  (let ((h (slot-ref c 'h)))
    (if (not h)
        (values)
        (if (is-a? c <pf>)
            (vhash-fold
             (lambda (k v s)
               (hash-set! t (p k) #t))
             #f h)
            (hash-for-each
             (lambda (k v)
               (hash-set! t (p k) #t))
             h)))))

(define (find-in o h c)
  (begin
    (when o
      (let ((slots (slot-ref o 'slots)))
        (if (or (null? slots) (pair? slots))
            (for-each (lambda (x)
                        (hash-set! h (p (car x)) #t))
                      (slot-ref o 'slots))
            (hash-for-each (lambda ( k v) (hash-set! h (p k) #t)) slots))))
    (chash-for-each h c)))
  
(define (find-in-mro o h l)
  (let lp ((l l))
    (if (pair? l)
        (begin
          (find-in o h (car l))
          (lp (cdr l))))))
        

(define (dirit o)
  (let ((h (make-hash-table)))
    (let ((slots (slot-ref o 'slots)))
      (if (or (null? slots) (pair? slots))
          (for-each (lambda (x) (hash-set! h (p (car x)) #t)) slots)
          (hash-for-each (lambda ( k v) (hash-set! h (p k) #t)) slots)))
                    
    (find-in-mro #f h (find-in-class o '__mro__ (list o)))
    (aif cl (find-in-class o '__class__ #f)
         (find-in-mro o h (find-in-class cl '__mro__ (list cl)))
         #f)
    (let ((l (py-list)))
      (hash-for-each
       (lambda (k v)
         (pylist-append! l k))
       h)
      (pylist-sort! l)
      l)))

(set! (@@ (util scheme-python oop dict) _dirit) dirit)
  
(define-method (dir (o <p>))
  (if (fluid-ref in-p)
      (next-method)
      (cont
       (next-method)
       (aif it (ref o '__dir__)
            (with-fluids ((in-p #t))
              (if (pyobject? o)
                  (it)
                  (it o)))
            (with-fluids ((in-p #t))
              (dirit o))))))
                    
(define-method (dir (o <py-list>))
  (cont
   (next-method)
   (let ((l1 (pylist-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-hashtable>))
  (cont
   (next-method)
   (let ((l1 (pyhash-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-string>))
  (cont
   (next-method)
   (let ((l1 (pystring-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-int>))
  (cont
   (next-method)
   (let ((l1 (pyint-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-float>))
  (cont
   (next-method)
   (let ((l1 (pyfloat-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-complex>))
  (cont
   (next-method)
   (let ((l1 (pycomplex-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-bytes>))
  (cont
   (next-method)
   (let ((l1 (pybytes-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))

(define-method (dir (o <py-bytearray>))
  (cont
   (next-method)
   (let ((l1 (pybytesarray-listing)))
     (if (is-a? o <p>)
         (let* ((l2 (next-method))
                (l  (+ l1 l2)))
           (pylist-sort! l)
           l)
         l1))))
          
(define-method (dir (o <hashtable>   )) (pyhash-listing))
(define-method (dir (o <string>      )) (pystring-listing))
(define-method (dir (o <complex>     )) (pycomplex-listing))
(define-method (dir (o <real>        )) (pyfloat-listing))
(define-method (dir (o <integer>     )) (pyint-listing))
(define-method (dir (o <bytevector>  )) (pybytes-listing))
(define-method (dir)
  (let ((l '()))
    (module-for-each (lambda (m . u)
                       (set! l (cons (symbol->string m) l)))
                     (current-module))
    (let ((ret (to-pylist l)))
      (pylist-sort! ret)
      ret)))

  
