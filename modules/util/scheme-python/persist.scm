(define-module (util scheme-python persist)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop goops)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (persist persistance)
  #:re-export(pcopyable? deep-pcopyable? pcopy deep-pcopy name-object
			 name-object-deep)
  #:export (cpit))

;; Inorder to reduce dependencies we just have a stub here that does nothing
(define-syntax cpit
  (lambda (x)
    (syntax-case x ()
      ((_ <c> (o lam a))
       #'(values)))))
