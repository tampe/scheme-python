(define-module (util scheme-python set)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (oop goops)
  #:use-module (ice-9 format)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python dict)
  #:use-module (util scheme-python for)
  #:use-module (util scheme-python try)
  #:use-module (util scheme-python list)
  #:use-module (util scheme-python yield)
  #:use-module (util scheme-python persist)
  #:use-module (util scheme-python bool)
  #:export (py-set frozenset weak-set))

(define-class <set> () dict)
(name-object <set>)

(cpit <set>
      (o (lambda (o a)
	   (slot-set! o 'dict
		      (let ((h (make-py-hashtable)))
			(let lp ((a a))
			  (if (pair? a)
			      (begin
				(py-hash-set! h (caar a) (cdar a))
				(lp (cdr a))))))))
	 (list
	  (hash-fold (lambda (k v s) (cons (cons k v) s))
		     '()
		     (slot-ref o 'dict)))))


(define miss (list 'miss))

(define-method (< (o1 <set>) ( o2 <set>))
  (and (not (equal? o1 o2))
       (for ((k : o1)) ()
            (if (in k o2)
                (values)
                (break #f))
            #:final #t)))

(define-method (> (o1 <set>) ( o2 <set>))
  (and (not (equal? o1 o2))
       (for ((k : o2)) ()
            (if (in k o1)
                (values)
                (break #f))
            #:final #t)))

(define-method (<= (o1 <set>) ( o2 <set>))
  (for ((k : o1)) ()
       (if (in k o2)
           (values)
           (break #f))
       #:final #t))

(define-method (>= (o1 <set>) ( o2 <set>))
  (for ((k : o2)) ()
       (if (in k o1)
           (values)
           (break #f))
       #:final #t))

(define-method (in k (o <set>))
  (in k (slot-ref o 'dict)))

(define-method (len (o <set>))
  (for ((x : o)) ((n 0))
       (+ n 1)
       #:final n))
  
(define-python-class set (object <set>)
  (define __init__
    (case-lambda
      ((self)       
       (slot-set! self 'dict (make-py-hashtable)))
      ((self x)
       (let ((d (make-py-hashtable)))
         (slot-set! self 'dict d)
         (if (eq? x '())
             (values)
	     (for ((y : x)) ()
               (pylist-set! d y #t)))))))

  (define __bool__
    (lambda (self)
      (bool (slot-ref self 'dict))))
      
  (define pop
    (lambda (self)
      (call-with-values (lambda () (pylist-pop! (slot-ref self 'dict)))
        (lambda (k v) k))))
  
  (define  add
    (lambda (self k)
      (pylist-set! (slot-ref self 'dict) k #t)))

  (define copy
    (lambda (self)
      (let ((dict (py-copy (slot-ref self 'dict))))
        (set dict))))

  (define difference
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x d)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (set r))))

  (define difference_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x r)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (values))))
  
  (define discard
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (pylist-delete! r (car l))
                (lp (cdr l))))))))
  
  (define intersection
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                     (if (not (__contains__ y k))
                         (cons k dels)
                         dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l)))))
        (set r))))

  (define intersection_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                  (if (not (__contains__ y k))
                      (cons k dels)
                      dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l))))))))

  (define __iand__ intersection_update)
  
  (define isdisjoint
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict))
             (n1 (len r))
             (n2 (len x)))
        (if (< n2 n1)
            (let ((xx x))
              (set! x r)
              (set! r xx)))
        (for ((k v : r)) ()
             (if (in k x)
                 (break #f))
             #:final
             #t))))
  
  (define issubset
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict)))
        (for ((k v : r)) ()
          (if (not (__contains__ x k))
              (break #f))
          #:final
          #t))))

  (define issuperset
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (for ((x v : r)) ()
          (if (not (in x r))
              (break #f))
          #:final
          #t))))

  (define remove
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (if (not (in x r))
            (raise KeyError "missing key in set at remove")
            (pylist-delete! r x)))))

  (define symmetric_difference
    (lambda (self x)
      (union (difference self x) (difference x self))))
  
  (define symmetric_difference_update
    (lambda (self x)
      (difference_update self x)
      (update self (difference x self))))

  (define union
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (set r))))))
  
  (define update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))             
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k v : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (values))))))

  (define __ior__  update)
  (define __iadd__ symmetric_difference_update)
  (define __repr__
    (lambda (self)
      (let* ((r (py-keys (slot-ref self 'dict)))
             (n (len r))
             (l (to-list r)))
        (cond
         ((= n 0)
          (format #f "set([])"))
         (else
          (format #f "set([~a~{, ~a~}])" (car l) (cdr l)))))))

  (define __contains__
    (lambda (self x)
      (let* ((d (slot-ref self 'dict))
             (t (slot-ref d    't)))
        (not (eq? miss (py-hash-ref t x miss))))))

  (define __and__
    (lambda (self op)
      (intersection self op)))

  (define __or__
    (lambda (self op)
      (union self op)))

  (define __sub__
    (lambda (self op)
      (difference self op)))

  (define __xor__
    (lambda (self op)
      (symmetric_difference self op)))

  (define __lt__
    (lambda (self other)
      (for ((k : self)) ()
           (if (in k other)
               (values)
               (break #f))
           #:final #t)))

  (define __le__
    (lambda (self other)
      (and (not (__eq__ self other))
           (__le__ self other))))
  
  (define __ge__
    (lambda (self other)
      (not (__lt__ other self))))

  (define __gt__
    (lambda (self other)
      (not (__le__ other self))))
    
  (define __eq__
    (lambda (self x)
      (and
       (__lt__ self x)
       (__lt__ x    self))))
  
  (define __iter__
    (lambda (self)
      ((make-generator
        (lambda (yield)
          (for ((k v : (slot-ref self 'dict))) ()
               (yield k)
               (values))))))))

(define-python-class weak-set (object <set>)
  (define __init__
    (case-lambda
      ((self)       
       (slot-set! self 'dict (make-py-weak-key-hashtable)))
      ((self x)
       (let ((d (make-py-weak-key-hashtable)))
         (slot-set! self 'dict d)
         (if (eq? x '())
             (values)
	     (for ((y : x)) ()
	       (pylist-set! d y #t)))))))

  (define __bool__
    (lambda (self)
      (bool (slot-ref self 'dict))))
      
  (define pop
    (lambda (self)
      (call-with-values (lambda () (pylist-pop! (slot-ref self 'dict)))
        (lambda (k v) k))))
  
  (define  add
    (lambda (self k)
      (pylist-set! (slot-ref self 'dict) k #t)))

  (define copy
    (lambda (self)
      (let ((dict (py-copy (slot-ref self 'dict))))
        (set dict))))

  (define difference
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x d)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (set r))))

  (define difference_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((x : (car l))) ()
                     (when (in x r)
                       (pylist-delete! r x)))
                (lp (cdr l)))))
        (values))))
  
  (define discard
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (pylist-delete! r (car l))
                (lp (cdr l))))))))
  
  (define intersection
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                     (if (not (__contains__ y k))
                         (cons k dels)
                         dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l)))))
        (set r))))

  (define intersection_update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))
        (let lp ((l l))
          (if (pair? l)
              (let ((y (car l)))
                (for ((k v : r)) ((dels '()))
                  (if (not (__contains__ y k))
                      (cons k dels)
                      dels)
                  #:final
                  (let lp ((dels dels))
                    (if (pair? dels)
                        (begin
                          (pylist-delete! r (car dels))
                          (lp (cdr dels))))))
                (lp (cdr l))))))))

  (define isdisjoint
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict))
             (n1 (len r))
             (n2 (len x)))
        (if (< n2 n1)
            (let ((xx x))
              (set! x r)
              (set! r xx)))
        (for ((k v : r)) ()
             (if (in k x)
                 (break #f))
             #:final
             #t))))
  
  (define issubset
    (lambda (self x)
      (let* ((r  (slot-ref self 'dict)))
        (for ((k v : r)) ()
          (if (not (__contains__ x k))
              (break #f))
          #:final
          #t))))

  (define issuperset
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (for ((x v : r)) ()
          (if (not (in x r))
              (break #f))
          #:final
          #t))))

  (define remove
    (lambda (self x)
      (let* ((r (slot-ref self 'dict)))
        (if (not (in x r))
            (raise KeyError "missing key in set at remove")
            (pylist-delete! r x)))))

  (define symmetric_difference
    (lambda (self x)
      (union (difference self x) (difference x self))))
  
  (define symmetric_difference_update
    (lambda (self x)
      (difference_update self x)
      (update self (difference x self))))

  (define union
    (lambda (self . l)
      (let* ((d (slot-ref self 'dict))
             (r (py-copy d)))
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (set r))))))
  
  (define update
    (lambda (self . l)
      (let* ((r (slot-ref self 'dict)))             
        (let lp ((l l))
          (if (pair? l)
              (begin
                (for ((k v : (car l))) ()
                     (pylist-set! r k #t))
                (lp (cdr l)))
              (values))))))
  
  (define __repr__
    (lambda (self)
      (let* ((r (py-keys (slot-ref self 'dict)))
             (n (len r))
             (l (to-list r)))
        (cond
         ((= n 0)
          (format #f "set([])"))
         (else
          (format #f "set([~a~{, ~a~}])" (car l) (cdr l)))))))

  (define __contains__
    (lambda (self x)
      (let* ((d (slot-ref self 'dict))
             (t (slot-ref d    't)))
        (not (eq? miss (py-hash-ref t x miss))))))

  (define __and__
    (lambda (self op)
      (intersection self op)))

  (define __or__
    (lambda (self op)
      (union self op)))

  (define __sub__
    (lambda (self op)
      (difference self op)))

  (define __xor__
    (lambda (self op)
      (symmetric_difference self op)))

  (define __lt__
    (lambda (self other)
      (for ((k : self)) ()
           (if (in k other)
               (values)
               (break #f))
           #:final #t)))

  (define __le__
    (lambda (self other)
      (and (not (__eq__ self other))
           (__le__ self other))))
  
  (define __ge__
    (lambda (self other)
      (not (__lt__ other self))))

  (define __gt__
    (lambda (self other)
      (not (__le__ other self))))
    
  (define __eq__
    (lambda (self x)
      (and
       (__lt__ self x)
       (__lt__ x    self))))
  
  (define __iter__
    (lambda (self)
      ((make-generator
        (lambda (yield)
          (for ((k v : (slot-ref self 'dict))) ()
               (yield k)
               (values))))))))

(define py-set set)
(define-python-class frozenset (set))
