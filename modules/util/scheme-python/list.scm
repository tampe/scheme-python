(define-module (util scheme-python list)
  #:use-module (ice-9 match)
  #:use-module (ice-9 control)
  #:use-module (ice-9 format)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (oop goops)
  #:use-module ((util scheme-python oop dict) #:select (int))
  #:use-module (util scheme-python hash)
  #:use-module (util scheme-python tuple)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python yield)
  #:use-module (util scheme-python for)
  #:use-module (util scheme-python try)
  #:use-module (util scheme-python bool)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python persist)
  #:re-export (pylist-ref pylist-set! pylist-delete!)
  #:export (to-list to-pylist <py-list> py-list
            pylist-append! pylist-remove!
            pylist-slice pylist-subset! pylist-reverse!
            pylist-pop! pylist-count pylist-extend! len 
            pylist-insert! pylist-sort!
            pylist-index pylist-null py-clear
            pylist pylist-listing Slice += py+=
            py-all py-any py-reversed
            *= +=
            py*= py+=
            ))


(define-python-class Slice    ())
(define-syntax-rule (pset o n val . rest)
  (cond
   ((list? n)
    (if (> (length n) 1)
        (pylist-set! (pylist-ref o (car n)) (cdr n) val)
        (pylist-set! o (car n) val)))
   ((vector? n)
    (pylist-subset! o (vector-ref n 0) (vector-ref n 1) (vector-ref n 2) val))
   (else
    (let () . rest))))

(define-syntax-rule (pref o n . rest)
  (cond
   ((list? n)
    (if (> (length n) 1)
        (pylist-ref (pylist-ref o (car n)) (cdr n))
        (pylist-ref o (car n))))
   ((vector? n)
    (pylist-slice o (vector-ref n 0) (vector-ref n 1) (vector-ref n 2)))
   
   (else
    (let () . rest))))

(define-method (+ (x <null>) (y <pair>))
  (let lp ((l y))
    (if (pair? l)
	(cons (car l) (lp (cdr l)))
	'())))

(define-method (+ (x <pair>) (y <null>))
  (let lp ((l x))
    (if (pair? l)
	(cons (car l) (lp (cdr l)))
	'())))

(define-method (in x (y <null>)) #f)
(name-object in)

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-class <py-list>  () vec nn)
(name-object <py-list>)

(cpit <py-list> (o (lambda (o n l)
		      (slot-set! o 'nn n)
		      (slot-set! o 'vec (list->vector l)))
		   ((@ (guile) list)
		    (slot-ref o 'nn)
		    (vector->list (slot-ref o 'vec)))))

(define (int x) x)

(define-method (pylist-delete! (o <py-list>) k)
  (let* ((n (slot-ref o 'nn))
         (k (int k))
         (k (if (< k 0) (+ k n) k)))    
    (pylist-subset! o k (+ k 1) None pylist-null)))

(define-method (pylist-delete! (o <py>) k)
  (aif it (ref-in-class o '__delitem__)
       (it o k)
       (next-method)))

(name-object pylist-delete!)

(define pylist-null
  (let ((o (make <py-list>)))
    (slot-set! o 'vec (make-vector 0))
    (slot-set! o 'nn   0)
    o))

(define-method (py-hash (o <py-list>))
  (let ((n (min complexity (slot-ref o 'nn)))
        (v (slot-ref o 'vec)))
    (let lp ((i 0) (s 0))
      (if (< i n)
          (lp (+ i 1)
              (xy (py-hash (vector-ref v i)) s))
          s))))

(define-method (to-list x)
  (if (null? x)
      x
      (if (pair? x)
          x
          (if (bool x)
              (for ((i : x)) ((r '()))
                   (cons i r)
                   #:final (reverse r))
              '()))))

(define-method (to-list (x <py>))
  (aif it (ref-in-class x '__tolist__)
       (it x)
       (next-method)))
(name-object to-list)

(defpair (to-list x) x)

(define-method (to-list (x <yield>))
  (define l '())
  (catch 'python
    (lambda ()
      (let lp ()
        (set! l (cons (next x) l))
        (lp)))
    (lambda (tag c v t)
      (if (eq? c StopIteration)
          (reverse l)
          (throw 'python c v t)))))

(define-method (to-list (x <py-list>))
  (let ((vec (slot-ref x 'vec))
        (n   (slot-ref x 'nn)))
    (let lp ((i 0))
      (if (< i n)
          (cons (vector-ref vec i) (lp (+ i 1)))
          '()))))

(define-method (to-pylist (l <py-list>))
  l)
(name-object to-pylist)

(defpair (to-pylist l)
  (let* ((n   (length l))
         (vec (make-vector (* 2 n)))
         (o   (make <py-list>)))
    
    (let lp ((l l) (i 0))
      (if (pair? l)
          (begin
            (vector-set! vec i (car l))
            (lp (cdr l) (+ i 1)))))
    
    (slot-set! o 'nn   n)
    (slot-set! o 'vec vec)
    o))

(define-method (to-pylist (l <vector>))
  (to-pylist (vector->list l)))

(define-method (to-pylist (o <string>))
  (to-pylist (string->list o)))

(define-method (bool (o <py-list>))
  (if (= (len o) 0)
      #f
      o))

(define-method (bool (o <vector>))
    (if (= (len o) 0)
      #f
      o))
(define-method (bool (o <string>))
    (if (= (len o) 0)
      #f
      o))

(define-method (to-pylist l)
  (if (null? l)
      (let ((o (make <py-list>)))
        (slot-set! o 'vec (make-vector 4))
        (slot-set! o 'nn   0)
        o)
      (error "not able to make a pylist")))

;;; REF
(define-method (pylist-ref (o <py-list>) nin)
  (pref o nin
    (define N (slot-ref o 'nn))
    (define n0 (int nin))
    (define n (if (< n0 0) (+ N n0) n0))
    (if (and (>= n 0) (< n (slot-ref o 'nn)))
        (vector-ref (slot-ref o 'vec) n)
        (raise IndexError))))

(defpair (pylist-ref o n)
  (pref o n
    (list-ref o (let ((n (int n))) (if (< n 0) (+ (length o) n) n)))))
  
(define-method (pylist-ref (o <vector>) n)
  (pref o n
    (vector-ref o (let ((n (int n))) (if (< n 0) (+ (vector-length o) n) n)))))

;;; SET
(define-method (pylist-set! (o <py-list>) nin val)
  (pset o nin val
    (define N (slot-ref o 'nn))
    (define n0 (int nin))
    (define n (if (< n0 0) (+ N n0) n0))
    
    (if (and (>= n 0) (< n (slot-ref o 'nn)))
        (vector-set! (slot-ref o 'vec) n val)
        (raise IndexError))))
(name-object pylist-set!)

(defpair (pylist-set! o n val)
  (pset o n val
    (list-set! o (let ((n (int n))) (if (< n 0) (+ (length o) n) n)) val)))

(define-method (pylist-set! (o <vector>) n val)
  (pset o n val
    (vector-set! o (let ((n (int n))) (if (< n 0) (+ (length o) n) n)) val)))


;;SLICE
(define-method (pylist-slice (o <py>) . l)
  (aif it (ref-in-class o '__getitem__)
       (it o (apply vector l))
       (next-method)))
(name-object pylist-slice)

(define-method (pylist-slice (o <py-list>) n1 n2 n3)
  (define N (slot-ref o 'nn))
  (define (f n)
    (let ((x (if (< n 0) (+ N n) n)))
      (if (< x 0)
          0
          (if (> x N)
              N
              x))))
  (let* ((n1   (f (if (eq? n1 None) 0                n1)))
         (n2   (f (if (eq? n2 None) (slot-ref o 'nn)  n2)))
         (n3   (f (if (eq? n3 None) 1                n3)))

         (vec  (slot-ref o 'vec))
         (l    (let lp ((i n1))
                 (if (< i n2)
                     (cons (vector-ref vec i) (lp (+ i n3)))
                     '()))))
    (to-pylist l)))

(define-method (pylist-slice (o <string>) n1 n2 n3)
  (define N (string-length o))
  (define (f n)
    (let ((x (if (< n 0) (+ N n) n)))
      (if (< x 0)
          0
          (if (> x N)
              N
              x))))
    
  (let* ((n1   (f (if (eq? n1 None) 0                  n1)))
         (n2   (f (if (eq? n2 None) (string-length o)  n2)))
         (n3   (f (if (eq? n3 None) 1                  n3))))
    (list->string
     (to-list
      (pylist-slice (to-pylist o) n1 n2 n3)))))


(defpair (pylist-slice o n1 n2 n3)
  (to-list
   (pylist-slice (to-pylist o) n1 n2 n3)))

(define-method (pylist-slice (o <vector>) n1 n2 n3)
  (list->vector
   (to-list
    (pylist-slice (to-pylist o) n1 n2 n3))))

(define-method (pylist-slice o n1 n2 n3)
  (if (null? o)
      (to-list (pylist-slice (to-pylist o) n1 n2 n3))
      (raise (TypeError "cannot slice this type"))))
  
;;SUBSET
(define-method (pylist-subset! (o <py>) n1 n2 n3 val)
  (aif it (ref-in-class o '__setitem__)
       (it o (vector n1 n2 n3) val)
       (next-method)))
(name-object pylist-subset!)

(define-method (pylist-subset! (o <py-list>) n1 n2 n3 val)
  (define N (slot-ref o 'nn))
  (define (f n) (if (< n 0) (+ N n) n))
  
  (let* ((n1   (f (if (eq? n1 None) 0                n1)))
         (n2   (f (if (eq? n2 None) (slot-ref o 'nn)  n2)))
         (n3   (f (if (eq? n3 None) 1                n3)))         
         (vec  (slot-ref o 'vec))
         (o2   (to-pylist val))
         (N2   (slot-ref o2 'nn))
         (vec2 (slot-ref o2 'vec)))
    (if (<= n2 N)
        (let lp ((i 0) (j n1))
          (if (< j n2)
              (if (< i N2)
                  (begin
                    (vector-set! vec j (vector-ref vec2 i))
                    (lp (+ i 1) (+ j n3)))
                  (let lp ((j2 j))
                    (if (< j2 n2)
                        (lp (+ j2 n3))
                        (let lp ((k1 j) (k2 j2))
                          (if (< k2 N)
                              (begin
                                (vector-set! vec k1 (vector-ref vec k2))
                                (lp (+ k1 1) (+ k2 1)))
                              (begin
                                (let lp ((i k2))
                                  (if (< i N)
                                      (begin
                                        (vector-set! vec i #f)
                                        (lp (+ i 1)))
                                      (slot-set! o 'nn k1)))))))))))
        (raise IndexError))
    (values)))
               

;;APPEND
(define-method (pylist-append! (o <py-list>) val)
  (let* ((n   (slot-ref o 'nn))
         (vec (slot-ref o 'vec))
         (N   (vector-length vec)))
    (if (< n N)
	(vector-set! vec n val)
        (let* ((N    (* 2 N))
               (vec2 (make-vector N)))
          (let lp ((i 0))
            (if (< i n)
                (begin
                  (vector-set! vec2 i (vector-ref vec i))
                  (lp (+ i 1)))))
          (vector-set! vec2 n val)
          (slot-set! o 'vec vec2)))
    (slot-set! o 'nn (+ n 1))
    (values)))
(name-object pylist-append!)

(define-method (pylist-append! o n)
  (raise 'nnotSupportedOP '__append__))

(define-method (pylist-append! (o <py>) n . l)
  (aif it (ref-in-class o 'append)
       (apply it o n l)
       (next-method)))
    
    

(define-method (write (o <py-list>) . l)
  (define port (if (null? l) #t (car l)))
    (let* ((l (to-list o)))      
    (if (null? l)
        (format port "[]")
        (format port "[~s~{, ~s~}]" (car l) (cdr l)))))
  
(define-method (display (o <py-list>) . l)
  (define port (if (null? l) #t (car l)))

  (let* ((l (to-list o)))      
    (if (null? l)
        (format port "[]")
        (format port "[~s~{, ~s~}]" (car l) (cdr l)))))


(define-method (+ (o1 <py-list>) (o2 <py-list>))
  (let* ((vec1 (slot-ref o1 'vec))
         (vec2 (slot-ref o2 'vec))        
         (n1   (slot-ref o1 'nn))
         (n2   (slot-ref o2 'nn))
         (n    (+ n1 n2))
         (vec  (make-vector (* 2 n)))
         (o    (make <py-list>)))

    (let lp ((i1 0))
      (if (< i1 n1)
          (begin
            (vector-set! vec i1 (vector-ref vec1 i1))
            (lp (+ i1 1)))
          (let lp ((i2 0) (i i1))
            (if (< i2 n2)
                (begin
                  (vector-set! vec i (vector-ref vec2 i2))
                  (lp (+ i2 1) (+ i 1)))))))
    
    (slot-set! o 'nn   n  )
    (slot-set! o 'vec vec)
    o))


(define-method (+ (o1 <pair>) (o2 <pair>))
  (append o1 o2))

(define-method (+ (o1 <py-tuple>) o2)
  (+ (slot-ref o1 'l) o2))

(define-method (+ o2 (o1 <py-tuple>))
  (+ o2 (slot-ref o1 'l)))

(define-method (+ (o1 <string>) (o2 <string>))
  (string-append o1 o2))

(define-method (+ (o1 <symbol>) (o2 <symbol>))
  (string->symbol
   (string-append
    (symbol->string o1)
    (symbol->string o2))))

(define-method (* (x <integer>) (o1 <py-list>)) (* o1 x))
(define-method (* (o1 <py-list>) (x <integer>))
  (let* ((vec  (slot-ref o1 'vec))
         (n    (slot-ref o1 'nn))
         (n2   (* n x))
         (vec2 (make-vector (* 2 n2)))
         (o    (make <py-list>)))

    (let lp1 ((i 0) (j 0))
      (if (< i x)
          (let lp2 ((j j) (k 0))
            (if (< k n)
                (begin
                  (vector-set! vec2 j (vector-ref vec k))
                  (lp2 (+ j 1) (+ k 1)))
                (lp1 (+ i 1) j)))))
    
    (slot-set! o 'nn   n2  )
    (slot-set! o 'vec vec2)
    o))

(define-method (* (x <integer>) (vec <string>)) (* vec x))
(define-method (* (vec <string>) (x <integer>))
  (let* ((n    (string-length vec))
         (n2   (* n x))
         (vec2 (make-string n2)))

    (let lp1 ((i 0) (j 0))
      (if (< i x)
          (let lp2 ((j j) (k 0))
            (if (< k n)
                (begin
                  (string-set! vec2 j (string-ref vec k))
                  (lp2 (+ j 1) (+ k 1)))
                (lp1 (+ i 1) j)))))
    vec2))

(define-method (* (x <integer> ) (l <pair>)) (* l x))
(define-method (* (x <py-tuple>) l) (* (slot-ref x 'l) l))
(define-method (* l (x <py-tuple>)) (* l (slot-ref x 'l)))
(define-method (* (l <pair>) (x <integer>))
  (let lp1 ((i 0))
    (if (< i x)
        (let lp2 ((k l))
            (if (pair? k)
                (cons (car k) (lp2 (cdr k)))                      
                (lp1 (+ i 1))))
        '())))


;;REVERSE
(define-method (pylist-reverse! (o <py-list>))
  (let* ((N   (slot-ref o 'nn))
         (M   (- N 1))
         (n   (floor-quotient N 2))
         (vec (slot-ref o 'vec)))
    (let lp ((i 0))
      (if (< i n)
          (let ((swap (vector-ref vec i))
                (k (- M i)))
            (vector-set! vec i (vector-ref vec k))
            (vector-set! vec k swap)
            (lp (+ i 1)))))))
(name-object pylist-reverse!)

(define-method (pylist-reverse! (o <py>) . l)
  (aif it (ref-in-class o 'reverse) 
       (apply o it l)
       (next-method)))

;;POP!
(define-method (pylist-pop! (o <py-list>) . l)
  (let ((index (if (null? l)
                   #f
                   (car l))))
    (if index
        (let ((x (pylist-ref o index)))
          (pylist-delete! o index)
          x)                                 
        (let* ((n   (slot-ref o 'nn))
               (m   (- n 1))
               (vec (slot-ref o 'vec)))
          (if (> n 0)
              (let ((ret (vector-ref vec m)))
                (slot-set! o 'nn m)
                (vector-set! vec m #f)
                ret)
              (raise IndexError "pop from empty list"))))))
(name-object pylist-pop!)

(define-method (pylist-pop! (o <py>) . l)
  (aif it (ref-in-class o 'pop) 
       (apply it o l)
       (next-method)))

;;COUNT
(define-method (pylist-count (o <py-list>) q)
  (let* ((n   (slot-ref o 'nn))
         (vec (slot-ref o 'vec)))
    (let lp ((i 0) (sum 0))
      (if (< i n)
          (if (equal? (vector-ref vec i) q)
              (lp (+ i 1) (+ sum 1))
              (lp (+ i 1) sum      ))
          sum))))
(name-object pylist-count)

(define-method (pylist-count (s <string>) q)
  (let* ((n   (string-length s))
         (q   (if (and (string? q) (= (string-length q) 1))
                  (string-ref q 0))))
    (let lp ((i 0) (sum 0))
      (if (< i n)
          (if (eq? (string-ref s i) q)
              (lp (+ i 1) (+ sum 1))
              (lp (+ i 1) sum      ))
          sum))))

(defpair (pylist-count l q)
    (let lp ((l l) (sum 0))
      (if (pair? l)
          (if (eq? (car l) q)
              (lp (cdr l) (+ sum 1))
              (lp (cdr l) sum      ))
          sum)))

(define-method (pylist-count (o <py>) . l)
  (aif it (ref-in-class o 'count) 
       (apply it o l)
       (next-method)))

;; extend!
(define-method (pylist-extend! (o <py-list>) iter)
  (for ((x : iter)) ()
       (pylist-append! o x)))
(name-object pylist-extend!)

(define-method (pylist-extend! (o <py>) . l)
  (aif it (ref-in-class o 'extend) 
       (apply it o l)
       (next-method)))

;; equal?
(define-method (py-equal? (o1 <py-list>) (o2 <py-list>))
  (equal o1 o2))

(define (equal o1 o2)
  (let ((n1   (slot-ref o1 'nn))
        (n2   (slot-ref o2 'nn))
        (vec1 (slot-ref o1 'vec))
        (vec2 (slot-ref o2 'vec)))
    (and
     (equal? n1 n2)
     (let lp ((i 0))
       (if (< i n1)
           (and (equal? (vector-ref vec1 i) (vector-ref vec2 i))
                (lp (+ i 1)))
           #t)))))

(define-class <py-seq-iter>  () o i n d)
(define-class <py-list-iter> (<py-list>) i d)

(name-object <py-seq-iter>)
(name-object <py-list-iter>)

(cpit <py-list-iter> (o (lambda (o i d)
			  (slot-set! o 'i i)
			  (slot-set! o 'd d))
			(list
			 (slot-ref o 'i)
			 (slot-ref o 'd))))

(cpit <py-seq-iter> (o (lambda (o oo i n d)
			 (slot-set! o 'o oo)
			 (slot-set! o 'i i)
			 (slot-set! o 'nn i)
			 (slot-set! o 'd d))
		       (list
			(slot-ref o 'o)
			(slot-ref o 'i)
			(slot-ref o 'nn)
			(slot-ref o 'd))))



(define-method (write (o <py-list-iter>) . l)
  (define port (if (null? l) #t (car l)))
  (for ((x : o)) ((l '()))
       (cons x l)
       #:final
       (let ((l (reverse l)))
         (if (null? l)
             (format port "iter[]")
             (format port "iter[~a~{, ~a~}]" (car l) (cdr l))))))

(define-method (write (o <py-seq-iter>) . l)
  (define port (if (null? l) #t (car l)))
  (for ((x : o)) ((l '()))
       (cons x l)
       #:final
       (let ((l (reverse l)))
         (if (null? l)
             (format port "iter[]")
             (format port "iter[~a~{, ~a~}]" (car l) (cdr l))))))


;;WRAP-IN
(define-method (wrap-in (o <py-list>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   (slot-ref o 'nn  ))
    (slot-set! out 'vec  (slot-ref o 'vec))
    (slot-set! out 'i   0)
    (slot-set! out 'd   1)
    out))

(define (make-vec v n i d)
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   n)
    (slot-set! out 'vec  v)
    (slot-set! out 'i    i)
    (slot-set! out 'd    v)
    out))

(set! (@@ (util scheme-python for) make-vec) make-vec)

(define-method (wrap-in (o <vector>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   (vector-length o))
    (slot-set! out 'vec o)
    (slot-set! out 'i   0)
    (slot-set! out 'd   1)
    out))

(define-method (py-reversed (o <py-list>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'i   (- (slot-ref o 'nn) 1))
    (slot-set! out 'vec (slot-ref o 'vec))
    (slot-set! out 'nn   (slot-ref o 'nn))
    (slot-set! out 'd   -1)
    out))
(name-object py-reversed)

(define-method (py-reversed (o <py>))
  (aif it (ref-in-class o '__reversed__)
       (it o)
       (let* ((a (ref-in-class o '__getitem__))
              (a (if a (lambda (x) (a o x)) a))
              (n (ref-in-class o '__len__))
              (n (if n (lambda () (n o)) n)))
         (if (and a n)
             (let ((ret (make <py-seq-iter>)))
               (slot-set! ret 'o a)
               (slot-set! ret 'i (n))
               (slot-set! ret 'nn -1)
               (slot-set! ret 'd -1))
             (next-method)))))

(define-syntax-rule (e x) (catch #t (lambda () x) (lambda xx (pk xx) #f)))

(define-method (wrap-in (o <py>))
  (aif it (ref-in-class o '__iter__)
       (let ((x (it o)))
         (cond
          ((pair? x)
           (wrap-in x))
          (else
           x)))
       (let* ((a (ref-in-class o '__getitem__))
              (a (if a (lambda (x) (a o x)) a)))
         (if a
             (let ((ret (make <py-seq-iter>)))
               (slot-set! ret 'o a)
               (slot-set! ret 'i 0)
               (slot-set! ret 'nn -1)
               (slot-set! ret 'd 1))
             (next-method)))))


(define-method (wrap-in (o <py-list-iter>))
  (let ((out (make <py-list-iter>)))
    (slot-set! out 'nn   (slot-ref o 'nn  ))
    (slot-set! out 'vec  (slot-ref o 'vec ))
    (slot-set! out 'i    (slot-ref o 'i   ))
    (slot-set! out 'd    (slot-ref o 'd   ))
    out))
  

(define-method (wrap-in (o <py-seq-iter>))
  (let ((ret (make <py-seq-iter>)))
    (slot-set! ret 'o  (slot-ref o 'o))
    (slot-set! ret 'i  (slot-ref o 'i))
    (slot-set! ret 'nn (slot-ref o 'nn))
    (slot-set! ret 'd  (slot-ref o 'd))))
        
;;NEXT
(define-next ((o <py-seq-iter>))
  (let ((i (slot-ref o 'i))
        (d (slot-ref o 'd))
        (a (slot-ref o 'o)))
    
    (let ((r (a i)))
      (if (eq? r empty)
          r
          (let ((ii (+ i d)))
            (begin
              (slot-set! o 'i ii)
              r))))))
        
(define-next ((o <py-list-iter>))
  (let ((i   (struct-ref o 2))
        (d   (struct-ref o 3))
        (n   (struct-ref o 4))
        (vec (struct-ref o 0)))
    (if (> d 0)
        (if (< i n)
            (let ((ret (vector-ref vec i)))
              (slot-set! o 'i (+ i 1))
              ret)
            empty)
        (if (>= i 0)
            (let ((ret (vector-ref vec i)))
              (struct-set! o 2 (- i 1))
              ret)
            empty))))
  
;;INSERT
(define-method (pylist-insert! (o <py-list>) i val)
  (let* ((vec (slot-ref o 'vec))
         (n   (slot-ref o 'nn))
         (i   (if (< i 0) (+ n i) i)))
    (if (and (>= i 0) (<= i n))
        (let lp ((v val) (i i))
          (if (< i n)
              (let ((swap (vector-ref vec i)))
                (vector-set! vec i v)
                (lp swap (+ i 1)))
              (pylist-append! o v)))
        (raise IndexError "Wrong index in insert"))))
(name-object pylist-insert!)

(define-method (pylist-insert! (o <py>) . l)
  (aif it (ref-in-class o 'insert) 
       (apply it o l)
       (next-method)))

;;CLEAR
(define-method (py-clear (o <p>))
  (aif it (ref-in-class o 'clear) 
       (apply it o)
       (next-method)))

(define-method (py-clear (o <py-list>))
  (slot-set! o 'nn  0))

;;REMOVE
(define-method (pylist-remove! (o <py-list>) val)
  (let ((n   (slot-ref o 'nn  ))
        (vec (slot-ref o 'vec)))
    (let lp ((i 0))
      (if (< i n)
          (let ((r (vector-ref vec i)))
            (if (equal? r val)
                (pylist-subset! o i (+ i 1) 1 '())
                (lp (+ i 1))))
          (raise ValueError "list removal has no element to remove")))))
(name-object pylist-remove!)

(define-method (pylist-remove! (o <py>) . l)
  (aif it (ref-in-class o 'remove) 
       (apply it o l)
       (next-method)))

;; SORT!
(define (id x) x)
(define (sort- it key reverse)
  (catch #t
    (lambda ()      
      (for ((x : it)) ((l '()) (i 0))
           (values (cons ((@ (guile) list) (key x) i x) l)
                   (+ i 1))
           
           #:final
           (begin
             (let lp ((l (sort (reverse! l) (if reverse > <)))
                      (i 0))
               (if (pair? l)
                   (let ((x (car l)))
                     (pylist-set! it i (caddr x))
                     (lp (cdr l) (+ i 1))))))))
    (lambda x (raise (TypeError "problem in sorting layout")))))

(define-method (pylist-sort! (o <py-list>) . l)
  (apply
   (lambda* (#:key (key id) (reverse #f))
     (sort- o key reverse))
   l))
(name-object pylist-sort!)

(define-method (pylist-sort! (o <py>) . l)
  (aif it (ref-in-class o 'sort) 
       (apply it o l)
       (next-method)))

;; INDEX
(define-method (pylist-index (o <py-list>) val . l)
  (let* ((n   (slot-ref o 'nn  ))
         (vec (slot-ref o 'vec))
         (f   (lambda (m) (if (< m 0) (+ m n) m))))
    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x) n))
            ((x y)
             (values (f x) (f y)))))
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (let lp ((i n1))
              (if (< i n2)
                  (let ((r (vector-ref vec i)))
                    (if (equal? r val)
                        i
                        (lp (+ i 1))))
                  (raise ValueError "could not find value in index fkn")))
            (raise IndexError "index out of scop in index fkn"))))))
(name-object pylist-index)

(define-method (pylist-index (o <string>) val . l)
  (let* ((n   (string-length o))
         (f   (lambda (m) (if (< m 0) (+ m n) m)))
         (val (if (and (string? val) (> (string-length val) 0))
                  (string-ref val 0)
                  val)))
    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x) n))
            ((x y)
             (values (f x) (f y)))))
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (let lp ((i n1))
              (if (< i n2)
                  (let ((r (string-ref o i)))
                    (if (equal? r val)
                        i
                        (lp (+ i 1))))
                  (raise ValueError "could not find value in index fkn")))
            (raise IndexError "index out of scop in index fkn"))))))

(defpair (pylist-index o val . l)
  (let* ((n   (length o))
         (f   (lambda (m) (if (< m 0) (+ m n) m))))
    (call-with-values
        (lambda ()
          (match l
            (()
             (values 0 n))
            ((x)
             (values (f x) n))
            ((x y)
             (values (f x) (f y)))))
      (lambda (n1 n2)
        (if (and (>= n1 0) (>= n2 0) (< n1 n) (<= n2 n))
            (let lp ((i o))
              (if (pair? i)
                  (let ((r (car i)))
                    (if (equal? r val)
                        i
                        (lp (cdr i))))
                  (raise ValueError "could not find value in index fkn")))
            (raise IndexError "index out of scop in index fkn"))))))
                 
(define-method (pylist-index (o <py>) . l)
  (aif it (ref-in-class o 'index) 
       (apply it o l)
       (next-method)))


;; len


(defpair (len l)  (length l))
(define-method (len x)
  (if (null? x)
      0
      (error "not a suitable lengthof" x)))
(define-method (len (v <vector>))  (vector-length v))
(define-method (len (s <string>))  (string-length s))
(define-method (len (o <py-list>)) (slot-ref o 'nn))
(define-method (len (o <py>))
  (aif it (ref-in-class o '__len__)
       (it o)
       (next-method)))
(name-object len)

(define (bo x) (if x #t #f))
(define-method (in x (l <py-tuple>)) (bo (member x (slot-ref l 'l))))
(define-method (in x (l <pair>))     (bo  (member x l)))
(define-method (in x (l <vector>))
  (define n (vector-length l))
  (let lp ((i 0))
    (if (< i n)
        (if (equal? x (vector-ref l i))
            #t
            (lp (+ i 1)))
        #f)))

(define-method (in (x <string>) (s <string>))
  (if (string-contains s x) #t #f))

(define-method (in (x <char>) (s <string>))
  (let/ec ret
    (string-for-each
     (lambda (ch)
       (if (eq? ch x)
	   (ret #t)))
     s))
  #f)

(define-method (in x (o <py-list>))
  (define l (slot-ref o 'vec))
  (define n (slot-ref o 'nn))
  (let lp ((i 0))
    (if (< i n)
        (if (equal? x (vector-ref l i))
            #t
            (lp (+ i 1)))
        #f)))

(define-method (in x (o <py>))
  (aif it (ref-in-class o '__contains__)
       (it o x)
       (next-method)))

(define-syntax-rule (defgen (op r s o1 o2) code ...)
  (begin
    (define-method (op (o1 <py-list>) (o2 <py-list>)) code ...)
    (define-method (op (o1 <pair>)   (o2 <pair>  )) code ...)
    (define-method (op (o1 <py-tuple>) o2)
      (op (slot-ref o1 'l) o2))
    (define-method (op o2 (o1 <py-tuple>))
      (op o2 (slot-ref o1 'l)))
    (define-method (op (o1 <vector>) (o2 <vector>)) code ...)
    (define-method (op (o1 <py>)      o2)
      (aif it (ref-in-class o1 'r)
           (it o1 o2)
           (next-method)))
    (define-method (op o1 (o2 <py>))
      (aif it (ref-in-class o2 's)
           (it o2 o1)
           (next-method)))))

(defgen (< __le__ __gt__ o1 o2)
  (let ((n1 (len o1))
        (n2 (len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
      (if (< x1 x2)
          (break #t))
      (if (> x1 x2)
	  (break #f))
      #:final
      (< n1 n2))))

(defgen (<= __lt__ __ge__ o1 o2)
  (let ((n1 (len o1))
        (n2 (len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
      (if (< x1 x2)
          (break #t))
      (if (> x1 x2)
	  (break #f))

      #:final
      (<= n1 n2))))

(defgen (> __ge__ __lt__ o1 o2)
  (let ((n1 (len o1))
        (n2 (len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
      (if (> x1 x2)
          (break #t))
      (if (< x1 x2)
	  (break #f))

      #:final
      (> n1 n2))))

(defgen (>= __gt__ __le__ o1 o2)
  (let ((n1 (len o1))
        (n2 (len o2)))
    (for ((x1 : o1) (x2 : o2)) ()
         (if (> x1 x2)
             (break #t))
	 (if (< x1 x2)
	  (break #f))

         #:final
         (>= n1 n2))))

(define-python-class list (object <py-list>)
  (define __repr__
    (lambda (self)
      (write self #f)))
    
  (define  __init__
    (letrec ((__init__
              (case-lambda
                ((self)
                 (slot-set! self 'vec (make-vector 30))
                 (slot-set! self 'nn   0))
                ((self it)
                 (__init__ self)
                 (for ((i : it)) () (pylist-append! self i))))))
      __init__)))

(name-object list)

(define pylist list)

(define-method (py-class (o <py-list>) list))

(define (pylist-listing)
  (let ((l
         (to-pylist
          (map symbol->string
               '(append
                 count
                 clear
                 extend
                 index
                 pop
                 insert
                 remove
                 reverse
                 sort
                 __matmul__
                 __rmatmul__                 
                 __init__
                 __le__
                 __lt__
                 __gt__
                 __ge__
                 __ne__
                 __eq__
                 __len__
                 __init__
                 __add__
                 __sub__
                 __rsub__
                 __neg__
                 __mul__
                 __rmul__
                 __radd__
                 __repr__
                 __contains__
                 __getattr__
                 __setattr__
                 __delattr__
                 __delitem__
                 __setitem__
                 __iter__
                 )))))
    
    (pylist-sort! l)
    l))

(define (py-all x)
  (for ((i : x)) ()
       (unless i (break #f))
       #:final
       #t))

(define (py-any x)
  (for ((i : x)) ()
       (when i (break #t))
       #:final
       #f))

(define py-list list)

(set! (@@ (util scheme-python oop dict) to-list) to-list)

(define-method (scheme? (o <py-list>)) py-list)
(define-method (+ (o1 <py-list>) (o2 <pair>))
  (+ o1 (to-pylist o2)))
(define-method (+ (o2 <pair>) (o1 <py-list>))
  (+ (to-pylist o2) o1))
(define-method (+ (o1 <py-list>) (o2 <null>))
  (+ o1 (to-pylist o2)))
(define-method (+ (o2 <null>) (o1 <py-list>))
  (+ (to-pylist o2) o1))

(define-method (py+= var val set) (set (+ var val)))
(define-method (py+= (var <p>) val set)
  (aif it (ref var '__iadd__)
       (it val)
       (next-method)))

(define-method (py+= (var <py-list>) val set)
  (for ((x : val)) ()
       (pylist-append! var x)))

(define-syntax-rule (+= var val) (py+= var val (lambda (v) (set! var v))))

(define-method (py*= var val set) (set (* var val)))
(define-method (py*= (var <p>) val set)
  (aif it (ref var '__imull__)
       (it val)
       (next-method)))

(define-method (py*= (var <py-list>) (val <integer>) set)
  (cond
   ((< val 0)
    (error "wrong number in *= for python lists"))
   ((= val 0)
    (py-clear var))
   ((= val 1)
    (values))
   (else
    (let ((w0 (py-list var)))
      (let lp ((i val))
        (if (= i 0)
            (values)
            (py+= var w0 (lambda (s) (values)))))))))

(define-syntax-rule (*= var val) (py*= var val (lambda (v) (set! var v))))



   
