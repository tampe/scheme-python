(define-module (util scheme-python for)
  #:use-module (util scheme-python yield)
  #:use-module (oop pf-objects)
  #:use-module (util scheme-python exceptions)
  #:use-module (util scheme-python def)
  #:use-module (oop goops)
  #:use-module (ice-9 control)
  #:use-module (ice-9 match)
  #:use-module (util scheme-python persist)
  #:export (for break next next-sielent wrap-in define-next empty
                i-cons i-append i-product i-and i-cycle zip))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(eval-when (compile eval load)
  (define (generate-temporaries2 x)
    (map (lambda (x) (generate-temporaries x)) x)))

(define-syntax-parameter break (lambda (x) #f))

(define-syntax for
  (lambda (x)
    (syntax-case x (:)
      ((for (a ...) . l)
       #'(for #f (a ...) . l))
      
      ((for A (G ...) . l)
       (and
        (syntax-case #'A ()
          ((_ . _) #f)
          (_ #t))
        
        (or-map (lambda (x)
                  (let lp ((x x))
                    (syntax-case x (:)
                      ((: G)   #t)
                      ((: I G) #f)
                      ((_ . l) (lp #'l))
                      (()      #f))))                      
                #'(G ...)))
       (with-syntax ((((I ...) ...)
                      (map (lambda (x)
                             (let lp ((x x) (r '()))
                               (syntax-case x (:)
                                 ((: I G)
                                  (append (reverse r)  (list #': #'I #'G)))
                                 ((: G)
                                  (append (reverse r)
                                          (list
                                           #':
                                           (with-syntax
                                               (((I)
                                                 (generate-temporaries
                                                  (list #'G))))
                                             #'I)
                                           #'G)))
                                 ((X . L)
                                  (lp #'L (cons #'X r))))))
                           #'(G ...))))
         #'(for A ((I ...) ...) . l)))

      ((for A ((x ... : I G) ...) ((c n) ...) code ... #:final fin)
       #'(for-work A ((x ... : I G) ...) ((c n) ...) (code ...) fin values))
    
      ((for A ((x ... : I G) ...) ((c n) ...) code ... #:finally fin)
       #'(for-work A ((x ... : I G) ...) ((c n) ...) (code ...) fin values))

      ((for A ((x ... : I G) ...) ((c n) ...) code ... #:else fin)
       #'(for-work A ((x ... : I G) ...) ((c n) ...) (code ...) (values)
                   (lambda () fin)))
      
      ((for A ((x ... : I G) ...) ((c n)) code ...)
       #'(for A ((x ... : I G) ...) ((c n)) code ... #:final c))

      ((for A ((x ... : I G) ...) ((c n) ...) code ...)
       #'(for-work A ((x ... : I G) ...) ((c n) ...) (code ...)
                   (values) values)))))   

(define-syntax-rule (o I ... code) (oi (f I) ... code))
(define-syntax oi
  (lambda (x)
    (syntax-case x ()
      ((oi code)
       #'code)
      
      ((oi (f (fnext I x ...)) y . z)
       (with-syntax (((xx ...) (generate-temporaries #'(x ...))))
         #`(let ((f (lambda (xx ... . qqq)
                      (match (list xx ...) ((x ...) (oi y . z))))))
             (call-with-values
                 (lambda () (fnext I))
               #,(if (> (length (syntax->datum #'(x ...))) 1)             
                     #'(case-lambda
                         ((w)
                          (apply f w))
                         (w (apply f w)))
                     #'f))))))))
                     
(define range (lambda x x))
(define (make-vec v n i d) (values))

(define-syntax for-work
  (lambda (z)
    (define (wrap-continue lp code)
      (if (syntax->datum lp)
          #`(lambda () (let/ec lp code))
          #`(lambda ()              #,@code)))
      
    (syntax-case z ()
      ((for #f ((x : It (ra a))) ((c n) ...) (code ...) fin er)
       (eq? (syntax->datum #'ra) 'list)
       #'(let/ec lp-break
           (syntax-parameterize ((break (lambda (z)
                                          (syntax-case z ()
                                            ((_ . l)
                                             #'(lp-break . l))
                                            (_ #'lp-break)))))
             (let* ((aa (wrap-in a))
                    (N  (slot-ref aa 'nn))
                    (v  (slot-ref aa 'vec))
                    (i  (slot-ref aa 'i))
                    (d  (slot-ref aa 'd)))
               (let ((c n) ...)
                 (if (or
                      (= d 0)
                      (and (> d 0) (>= i N))
                      (and (< d 0) (<= i N)))
                     (begin er fin)
                     (let lp ((i i) (c c) ...)          
                       (let-syntax ((It (lambda (z)
                                          (syntax-case z ()
                                            ((_ . l)
                                             #'((make-vec v N (+ i d) d) . l))
                                            (_
                                             #'(make-vec v N (+ i d) d))))))
                         (if (= i N)
                             (begin er fin)
                             (let ((x (vector-ref v i)))
                               (call-with-values
                                   (lambda () code ...)
                                 (lambda z (apply lp (+ i d) z)))))))))))))
                       

      ((for #f ((x : It (ra a))) ((c n) ...) (code ...) fin er)
       (eq? (syntax->datum #'ra) 'tuple)
       #'(let/ec lp-break
           (syntax-parameterize ((break (lambda (z)
                                          (syntax-case z ()
                                            ((_ . l)
                                             #'(lp-break . l))
                                            (_ #'lp-break)))))
             (let ((aa a))
               (let lp ((xx aa) (c n) ...)          
                 (let-syntax ((It (lambda (z)
                                    (syntax-case z ()
                                      ((_ . l)
                                       #'((wrap-in (cdr aa)) . l))
                                      (_
                                       #'(wrap-in (cdr aa)))))))
                   (if (pair? xx)
                       (let ((x (car xx)))
                         (call-with-values
                             (lambda () code ...)
                           (lambda z (apply lp (cdr xx) z))))
                       (begin er fin))))))))
                                      
       

      ((for #f ((x : It (ra a))) ((c n) ...) (code ...) fin er)
       (eq? (syntax->datum #'ra) 'range)
       #'(let/ec lp-break
           (syntax-parameterize ((break (lambda (z)
                                          (syntax-case z ()
                                            ((_ . l)
                                             #'(lp-break . l))
                                            (_ #'lp-break)))))

             (let ((aa a))
               (let lp ((x 0) (c n) ...)
                 (let-syntax ((It (lambda (z)
                                    (syntax-case z ()
                                      ((_ . l)
                                       #'((range (+ x 1) aa) . l))
                                      (_
                                       #'(range (+ x 1) aa))))))

                   (if (< x aa)
                       (call-with-values
                           (lambda () code ...)
                         (lambda z (apply lp (+ x 1) z)))
                       (begin er fin))))))))

      ((for #f ((x : It (ra a1 a2))) ((c n) ...) (code ...) fin er)
       (eq? (syntax->datum #'ra) 'range)
       #'(let/ec lp-break
           (syntax-parameterize ((break (lambda (z)
                                          (syntax-case z ()
                                            ((_ . l)
                                             #'(lp-break . l))
                                            (_ #'lp-break)))))

             (let ((aa1 a1) (aa2 a2)) 
               (let lp ((x aa1) (c n) ...)
                 (let-syntax ((It (lambda (z)
                                    (syntax-case z ()
                                      ((_ . l)
                                       #'((range (+ x 1) aa2) . l))
                                      (_
                                       #'(range (+ x 1) aa2))))))

                   (if (< x aa2)
                       (call-with-values
                           (lambda () code ...)
                         (lambda z (apply lp (+ x 1) z)))
                       (begin er fin))))))))

      ((for #f ((x : It (ra a1 a2 -1))) ((c n) ...) (code ...) fin er)
       (eq? (syntax->datum #'ra) 'range)
       #'(let/ec lp-break
           (syntax-parameterize ((break (lambda (z)
                                          (syntax-case z ()
                                            ((_ . l)
                                             #'(lp-break . l))
                                            (_ #'lp-break)))))

             (let ((aa1 a1) (aa2 a2))
               (let lp ((x aa1) (c n) ...)
                 (let-syntax ((It (lambda (z)
                                    (syntax-case z ()
                                      ((_ . l)
                                       #'((range (- x 1) aa2 -1) . l))
                                      (_
                                       #'(range (- x 1) aa2 -1))))))

                   (if (> x aa2)
                       (call-with-values
                           (lambda () code ...)
                         (lambda z (apply lp (- x 1) z)))
                       (begin er fin))))))))
                
      ((for continue ((x ... : It G) ...) ((c n) ...) (code ...) fin er)
       (with-syntax (((cc     ...) (generate-temporaries #'(c ...)))
                     ((f-next ...) (generate-temporaries #'(It ...))))
         #`(let/ec lp-break             
             (let* ((It     (wrap-in G)) ...
                    (f-next (resolve-method-g next (list (class-of It)))) ...)
               (syntax-parameterize ((break (lambda (z)
                                              (syntax-case z ()
                                                ((_ . l)
                                                 #'(lp-break . l))
                                                (_ #'lp-break)))))
                                                     

                 (let ((finale   (lambda (c ...) fin))
                       (cc       None)
                       ...)
                       
                   (catch 'python
                     (lambda ()
                       (let lp ((c n) ...)
                         (set! cc c) ...
                         (o (f-next It x ...) ...
                            (call-with-values
                                (lambda ()
                                  #,(if (syntax->datum #'continue)
                                        #'(let/ec continue code ...)
                                        #'(begin code ...)))
                              (lambda (cc ... . skip) (lp cc ...))))))
                     (lambda (tag E . u)
                       (if (eq? E StopIteration)
                           (begin (er) (finale cc ...))
                           (apply throw tag E u)))))))))))))

(define empty (list 'empty))

(define-syntax-rule (empty->stop code ...)
  (call-with-values (lambda () (begin code ...))
    (lambda (ret . l)
      (if (eq? ret empty)
          (throw 'python StopIteration)
          (apply values ret l)))))

(define-syntax-rule (>li x ...)
  (call-with-values (lambda () x ...)
    (lambda y y)))

(define-syntax define-next
  (lambda (x)
    (syntax-case x ()
      ((_ arg code ...)
       (with-syntax ((name1 (datum->syntax x 'next-sielent))
                     (name2 (datum->syntax x 'next)))         
         #'(begin
             (define-method (name1 . arg) code ...)
             (define-method (name2 . arg) (empty->stop code ...))))))))

(define-class <scm-list>   () l)
(define-class <scm-string> () s i)

(name-object <scm-list>)
(name-object <scm-string>)

(cpit <scm-list> (o (lambda (o l) (slot-set! o 'l l))
		    (list (slot-ref o 'l))))
(cpit <scm-string> (o (lambda (o s i)
			(slot-set! o 's s)
			(slot-set! o 'i i))
		      (list
		       (slot-ref o 's)
		       (slot-ref o 'i))))

(define-next (x) empty)

(define-next ((l <scm-list>))
  (let ((ll (struct-ref l 0)))
    (if (pair? ll)
        (begin
          (struct-set! l 0 (cdr ll))
          (car ll))
        empty)))

(define-next ((l <scm-string>))
  (let ((s (slot-ref l 0))
        (i (slot-ref l 1)))
    (if (= i (string-length s))
        empty
        (begin
          (slot-set! l 1 (+ i 1))
          (string-ref s i)))))

(define-next ((l <yield>))
  (let ((k (struct-ref l 1))
        (s (struct-ref l 0))
        (c (struct-ref l 2)))
    (if (not c)
        (if k
            (k (lambda (g)            
                 (if g
                     (next g)
                     'None)))
            (s))
        empty)))

(name-object next)

(define-method (wrap-in (o <yield>))
  o)

(define-method (wrap-in (o <p>))
  (aif it (ref o '__iter__)
       (let ((x (it)))
         (cond
          ((or (null? x) (pair? x)) (wrap-in x))
          (else      x)))
       (next-method)))


(define-next ((l <p>))
  (catch #t
    (lambda ()
      ((ref l '__next__)))
    (lambda (py C . l)
      (if (eq? C StopIteration)
          empty
          (apply throw py C l)))))

(define-method (wrap-in x)
  (cond
   ((or (null? x) (pair? x))
    (let ((o (make <scm-list>)))
      (slot-set! o 'l x)
      o))
   
   ((string? x)
    (let ((o (make <scm-string>)))
      (slot-set! o 's x)
      (slot-set! o 'i 0)
      o))
   
   (else
    x)))

(define-method (wrap-in (o <scm-list>))
  (let ((oo (make <scm-list>)))
    (slot-set! oo 'l (slot-ref o 'l))
    oo))

(define-method (wrap-in (o <scm-string>))
  (let ((oo (make <scm-string>)))
    (slot-set! oo 's (slot-ref oo 'l))
    (slot-set! oo 'i (slot-ref oo 'i))
    oo))

(name-object wrap-in)

(set! (@@ (oop dict) hashforeach)
  (lambda (f d)
    (for ((k v : d)) () (f k v))))

(define (mk-k x)
  (if (keyword? x)
      x
      (symbol->keyword
       (if (string? x)
	   (string->symbol x)
	   x))))

(set! (@@ (oop dict) mkw)
  (lambda (kw)    
    (for ((k v : kw)) ((l '()))
      (cons* v (mk-k k) l)
      #:final (reverse l))))

(define-class I-cons () x I)

  
(define-method (i-cons x I)
  (let ((C (make I-cons)))
    (slot-set! C 'x x)
    (slot-set! C 'I (wrap-in I))
    C))

(define-method (wrap-in (I I-cons))
  (let ((C (make I-cons)))
    (slot-set! C 'x (slot-ref I 'x))
    (slot-set! C 'I (wrap-in (slot-ref I 'I)))
    C))


(define-next ((I I-cons))
  (let ((x (struct-ref I 0)))
    (if (eq? x empty)
        (next-sielent (struct-ref I 1))
        (begin
          (slot-set! I 'x empty)
          x))))

(define-class I-append () I J)

(define i-append
  (case-lambda
    (()  empty)
    ((I) I)
    ((I  J)
     (let ((C (make I-append)))
       (slot-set! C 'I (wrap-in I))
       (slot-set! C 'J (wrap-in J))
       C))
    ((I . L)
     (i-append I (apply i-append L)))))
    

(define-method (wrap-in (I I-append))
  (let ((C (make I-append)))
    (slot-set! C 'I (wrap-in (slot-ref I 'I)))
    (slot-set! C 'J (wrap-in (slot-ref I 'J)))
    C))

(define-next ((I I-append))
  (let ((II (struct-ref I 0)))
    (if (eq? II empty)
        (next-sielent (struct-ref I 1))
        (let ((r (next-sielent II)))
          (if (eq? r empty)
              (begin
                (slot-set! I 'I empty)
                (next-sielent (slot-ref I 'J)))
              r)))))

(define-class I-product () l)
(define-method (i-product . l)
  (let ((C (make I-product)))
    (slot-set! C 'l l)
    C))

(define-method (wrap-in (I I-product))
  (let ((C (make I-product)))
    (slot-set! C 'l (map wrap-in (slot-ref I 'l)))
    C))

(define-next ((I I-product))
  (let ((l (struct-ref I 0)))
    (if (eq? l empty)
        empty
        (let ((ll (map next-sielent l)))
          (if (or-map (lambda (x) (eq? x empty)) ll)
              (begin
                (slot-set! I 'l empty)
                empty)
              (apply values ll))))))

(define-class I-ref () I)
(define-method (i-ref I)
  (let ((C (make I-ref)))
    (slot-set! C 'I I)
    C))

(define-method (wrap-in (I I-ref)) I)

(define-next ((I I-ref)) (next-sielent (struct-ref I 0)))

(define-class I-wrap () I)
(define (i-wrap I)
  (let ((C (make I-wrap)))
    (slot-set! C 'I I)
    C))


(define-class I-cycle () I G n)

(define (i-cycle G n)
  (let* ((C  (make I-cycle))
         (I  (wrap-in G))
         (G  (wrap-in I)))
    (slot-set! C 'I  I)
    (slot-set! C 'G  G)
    (slot-set! C 'n  n)
    C))

(define-method (wrap-in (I I-cycle))
  (let ((C (make I-cycle)))
    (slot-set! C 'n (slot-ref I 'n))
    (slot-set! C 'G (slot-ref I 'G))
    (slot-set! C 'I (wrap-in (slot-ref I 'I)))
    C))

(define-next ((I I-cycle))
  (let ((n  (slot-ref I 'n))
        (II (slot-ref I 'I)))
    (if (or (eq? n None) (> n 0))
        (let ((ret (next-sielent II)))
          (if (eq? ret empty)
              (if (eq? n None)
                  (let ((II (wrap-in (slot-ref I 'G))))
                    (slot-set! I 'I II)
                    (next-sielent II))
                  (begin
                    (slot-set! I 'n (- n 1))
                    (if (= n 1)
                        ret
                        (let ((II (wrap-in (slot-ref I 'G))))
                          (slot-set! I 'I II) 
                          (next-sielent II)))))
              ret))
        empty)))
                

(define-class I-and () l)
(define (i-and . l)
  (let ((C (make I-and)))
    (slot-set! C 'l (map wrap-in l))
    C))

(define-method (wrap-in (I I-and))
  (let ((C (make I-and)))
    (slot-set! C 'l (map wrap-in (slot-ref I 'l)))
    C))

(define-next ((I I-and))
  (let lp ((l (slot-ref I 'l)) (x empty))
    (if (pair? l)
        (let ((r (next-sielent (car l))))
          (if (eq? r empty)
              r
              (lp (cdr l) r)))
        x)))
       
                  
(define zip i-product)
               
