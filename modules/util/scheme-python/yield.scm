(define-module (util scheme-python yield)
  #:use-module (util scheme-python oop pf-objects)
  #:use-module (util scheme-python exceptions)
  #:use-module (oop goops)
  #:use-module (ice-9 control)
  #:use-module (ice-9 match)
  #:use-module (util scheme-python persist)
  #:replace (send)
  #:export (<yield> 
            in-yield define-generator
            make-generator
            sendException sendClose
            yield-from yield-f yield-raw))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define in-yield (make-fluid #f))
(name-object in-yield)

(define-syntax-parameter YIELD (lambda (x) #f))

(define-syntax yield-raw
  (lambda (x)
    (syntax-case x ()
      ((_ x ...)
       #'(begin
           (fluid-set! in-yield #t)
           (abort-to-prompt YIELD x ...)))
      (x
       #'(lambda x
           (fluid-set! in-yield #t)
           (apply abort-to-prompt YIELD x))))))

(define-syntax yield-g
  (lambda (x)
    (syntax-case x ()
      ((_ #f x ...)
       #'(begin
           (fluid-set! in-yield #t)
           ((yield-raw x ...) #f)))

      ((_ g x ...)
       #'(begin
           (fluid-set! in-yield #t)
           (g (yield-raw x ...))))
               
      (x
       #'(lambda (g . x)
           (fluid-set! in-yield #t)
           (if g
               (g (apply yield-raw x))
               ((apply yield-raw x) #f)))))))

(define-syntax yield
  (lambda (x)
    (syntax-case x ()
      ((_ x ...)
       #'(yield-g #f x ...))     
      (x
       #'(lambda x
           (apply yield-g #f x))))))


(define-syntax make-generator
  (syntax-rules ()
    ((_ closure)
     (lambda a
       (let ()
         (define obj   (make <yield>))
         (define ab (make-prompt-tag))
         (syntax-parameterize ((YIELD (lambda x #'ab)))
           (slot-set! obj 'k #f)
           (slot-set! obj 'closed #f)
           (slot-set! obj 's
             (lambda ()
               (call-with-prompt ab
                 (lambda ()
                   (let ((y yield))
                     (set-procedure-property! y 'yield-g yield-g)
                     (apply closure y a))
                   (slot-set! obj 'closed #t)
                   (throw 'python StopIteration (StopIteration) #f))
                 (letrec ((lam
                           (lambda (k . l)
                             (fluid-set! in-yield #f)
                             (slot-set! obj 'k
                                        (lambda (a)
                                          (call-with-prompt ab
                                            (lambda ()
                                              (k a))
                                            lam)))
                             (apply values l))))
                   lam))))
           obj))))))

(define-syntax define-generator
  (lambda (x)
    (syntax-case x ()
      ((_ (f . args) code ...)
       #'(define f
           (make-generator
            (lambda args
              code ...)))))))

(define-class <yield> () s k closed)
(cpit <yield> (o (lambda (obj s k closed)
		   (slot-set! obj 's      s     )
		   (slot-set! obj 'k      k     )
		   (slot-set! obj 'closed closed)
		   obj)
		 (list
		  (slot-ref o 's)
		  (slot-ref o 'k)
		  (slot-ref o 'closed))))

(name-object <yield>)
 
(define-method (send (l <yield>) . u)
  (let ((k (slot-ref l 'k))
        (s (slot-ref l 's))
        (c (slot-ref l 'closed)))
    (if (not c)
        (if k
            (k (lambda (g)
                 (if g
                     (apply send g u)
                     (if (null? u)
                         '()
                         (apply values u)))))
            (if (or (null? u) (eq? (car u) 'None))
                (s)
                (throw 'python TypeError
                       (TypeError
                    "can't send a non-None value to a just-started generator"
                        ) #f)))            
        (throw 'python StopIteration (StopIteration) #f))))

(define-method (send-g (l <yield>) g)
  (let ((k (slot-ref l 'k))
        (s (slot-ref l 's))
        (c (slot-ref l 'closed)))
    (if (not c)
        (if k
            (k g)
            (throw 'python TypeError
                   (TypeError
                    "can't send a non-None value to a just-started generator"
                    )))
        (throw 'python StopIteration (StopIteration) #f))))

(define-method (sendException (l <yield>) e o tr)
  (let ((k (slot-ref l 'k))
        (s (slot-ref l 's))
        (c (slot-ref l 'closed)))
    (if (not c)
        (if k           
            (k (lambda (g)
                 (if g
                     (sendException g e o tr)
                     (throw 'python e o tr))))
            (throw 'python Exception (Exception) tr))
        (throw 'python StopIteration (StopIteration) #f))))

(define-method (sendClose (l <yield>))
  (let ((k (slot-ref l 'k))
        (s (slot-ref l 's))
        (c (slot-ref l 'closed)))
    (if c
        (values)
        (if k
            (begin
              (catch #t              
                (lambda ()
                  (k (lambda (g)
                       (if g
                           (sendClose g)
                           (throw 'python GeneratorExit GeneratorExit #f))))
                  (slot-set! l 'closed #t)
                  (throw 'python RuntimeError RuntimeError #f))
                (lambda (tag . v)
                  (slot-set! l 'closed #t)
                  (if (eq? tag 'python)
                      (match v
                        ((tag . l)
                         (if (eq? tag GeneratorExit)
                           (values)
                           (apply throw tag l))))
                      (apply throw tag v))))
              (slot-set! l 'closed #t))))))
            

(define-method (send (l <p>) . u)
  (apply (ref l '__send__) u))

(define-method (sendException (l <p>) . u)
  (apply (ref l 'throw) u))

(define-method (sendClose (l <p>))
  ((ref l '__close__)))

(define (yield-from yield gen)
  (let ((yield-g (procedure-property yield 'yield-g)))
    (if yield-g
        (catch 'python
          (lambda ()
            (call-with-values
                (lambda () (send gen))
              (lambda ret
                (letrec ((up (lambda (from)
                               (call-with-values
                                   (lambda () (send-g gen from))
                                 (lambda ret
                                   (apply yield-g up ret))))))
                  (apply yield-g up ret)))))
          (lambda x
            (if (and (> (length x) 1) (eq? (cadr x) StopIteration))
                (values)
                (apply throw x))))
        (error "not a yield type in first argument of mk-yield-from"))))
  
                              
